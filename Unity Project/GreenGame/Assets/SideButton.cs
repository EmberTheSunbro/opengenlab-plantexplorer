using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SideButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    [SerializeField]
    Image hoverText;

 

    public void OnPointerEnter(PointerEventData eventData)
    {    
        hoverText.gameObject.GetComponent<Animator>().SetTrigger("Appear");
    }

    public void OnPointerExit(PointerEventData eventData)
    {     
        hoverText.gameObject.GetComponent<Animator>().SetTrigger("Appear");
    }
}
