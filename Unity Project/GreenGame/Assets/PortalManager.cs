using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalManager : MonoBehaviour
{

    [SerializeField]
    float portalEnableThresholdDistance, portalJumpThresholdDistance, cameraMoveSpeed;

    [SerializeField]
    GameObject portalDisplay;

    [SerializeField]
    GameObject tissueCameraMarker;

    [SerializeField]
    GameObject circle;

    [SerializeField]
    GameObject PS;


    // Update is called once per frame
    void Update()
    {
        //print(Vector3.Distance(GetComponent<RectTransform>().transform.position, GameManager.instance.gameCamera.transform.position));

        if (Vector3.Distance(GetComponent<RectTransform>().transform.position, GameManager.instance.gameCamera.transform.position) < portalEnableThresholdDistance)
        {
            portalDisplay.SetActive(true);
        }

        //if (Vector3.Distance(transform.position, cam.transform.position) < portalJumpThresholdDistance)
        //{
        //    cam.transform.position = renderTextureCam.transform.position;

        //    moveCam = false;

        //    circle.SetActive(true);

        //    PS.SetActive(true);
        //}

    }

    public void ZeroOutScale()
    {
        portalDisplay.transform.localScale = Vector3.zero;
        portalDisplay.SetActive(false);
    }

}
