using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SpecialZoomButton : SpecialSelectionButton //Inherit from special button script
{
    //Assign text to appear during the onpointer enter
    public Animator hoverPanelTextToActivate;

    bool currentlySelected = false; //if set true then this needs to become not selectable and also stay at alpha 1

    ZoomManager zm; //For refrencing the current toggle
    public Toggle thisToggle; //The toggle that this is connected to

    bool appeared = false;

    protected override void Awake()
    {
        base.Awake();
        
        zm = GameObject.Find("ZoomManager").GetComponent<ZoomManager>();
    }

    private void Update()
    {
        if(zm == null && SceneManager.GetActiveScene().name == "Microscope")
        {
            zm = GameObject.Find("ZoomManager").GetComponent<ZoomManager>();
        }
        else if(SceneManager.GetActiveScene().name == "Microscope")
        {
            if (GameManager.instance.mainMenuUI.activeSelf)
            {
                hoverPanelTextToActivate.SetTrigger("Appear");
            }

            //print("The current toggle is equal to this one : " + (zm.currentToggle == thisToggle) + ". currently selected : " + currentlySelected);

            //An update to check if the current zoom toggle == this buttons toggle
            if (zm.currentToggle == thisToggle && !currentlySelected) //If it is then this object can't be enabled and alpha gets set to 1
            {
                currentlySelected = true;

                Color tempColor = highlightedImage.color;
                tempColor.a = 1;
                highlightedImage.color = tempColor;
            }
            else if (zm.currentToggle != thisToggle && currentlySelected) //If it isn't already selected then the alpha gets set back to 0 and it becomes selectable
            {
                Color tempColor = highlightedImage.color;
                tempColor.a = 0;
                highlightedImage.color = tempColor;

                currentlySelected = false;
            }
        }
    }

    public override void PointerEnter()
    {
        //hoverPanelTextToActivate.SetTrigger("Appear");
        if (!currentlySelected)
        {
            base.PointerEnter();
        }
    }

    public override void PointerExit()
    {
        //hoverPanelTextToActivate.SetTrigger("Disappear");
        if (!currentlySelected)
        {
            base.PointerExit();
        }
    }

    public override void PointerClick()
    {
        AudioManager.instance.Play("Positive");
        base.PointerClick();
        thisToggle.isOn = true;       
    }


}
