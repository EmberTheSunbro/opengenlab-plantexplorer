using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuOpener : MonoBehaviour
{

    GameObject mainMenuUI;

    private void Start()
    {
        mainMenuUI = GameManager.instance.mainMenuUI;
    }

    public void OpenMenu()
    {
        mainMenuUI.SetActive(true);
    }
}
