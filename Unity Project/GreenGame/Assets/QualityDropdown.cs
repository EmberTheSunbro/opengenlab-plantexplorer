using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QualityDropdown : MonoBehaviour
{
    public Dropdown qualityDrop;

    public int lastQuality = 3;

    public void ChangeQuality(int newQuality)
    {
        if(newQuality != lastQuality)
        {
            lastQuality = newQuality;

            if (newQuality == 0)
            {
                QualitySettings.SetQualityLevel(0);
            }
            else
            {
                QualitySettings.SetQualityLevel(newQuality + 1);
            }

            print(QualitySettings.GetQualityLevel());
        }
    }


}
