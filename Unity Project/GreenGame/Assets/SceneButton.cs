using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SceneButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField]
    Image hoverImage;

    [SerializeField]
    Image hoverText;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnPointerEnter(PointerEventData eventData)
    {
       // print(EventSystem.current.currentSelectedGameObject.name);

        hoverImage.gameObject.GetComponent<Animator>().SetTrigger("Appear");

        hoverText.gameObject.GetComponent<Animator>().SetTrigger("Appear");

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        hoverImage.gameObject.GetComponent<Animator>().SetTrigger("Appear");

        hoverText.gameObject.GetComponent<Animator>().SetTrigger("Appear");
    }
}
