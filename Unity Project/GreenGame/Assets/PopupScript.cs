using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PopupScript : MonoBehaviour
{
    [System.NonSerialized] 
    public bool isFadingOut = false; //prevents fading out again if already fading out 

    Coroutine lastFadeCoroutine;

    private float fadeTime = 1f;

    public List<PopupCollider> popupColliders; //Check these to see if any are currently raycasted

    public List<Image> popupImages;
    public List<TextMeshProUGUI> popUpTexts;
    public List<GameObject> popupObjects; //Objects will be de-activated if active on popup and activated if not active on popup
    
    bool startDeactivated;

    bool fadedIn = false;

    public void Awake()
    {
        //Add any images or texts in popUpHierarchy to images and texts to be faded on this popup activation (For ease of setup)
        popupImages.AddRange(transform.GetComponentsInChildren<Image>()); 
        popUpTexts.AddRange(transform.GetComponentsInChildren<TextMeshProUGUI>());

        //De-activate all the images, texts, and objects
        PopupActivation(false);

        if(popupObjects.Count > 0)
        {
            startDeactivated = popupObjects[0].activeSelf ? true : false;
        }
    }

    private void Update()
    {
        bool currentlyRaycasted = false; 
        
        foreach (PopupCollider thisCollider in popupColliders) //Checks to see if any of the things that would trigger this popup are raycasted
        {
            if (thisCollider.raycasted)
            {
                currentlyRaycasted = true;
                break;
            }
        }

        //Start fading in if something is being raycasted, it hasn't faded in yet, and there isn't currently a popup open
        if (currentlyRaycasted && !fadedIn && GameManager.instance.popupOpen == null)
        {
            PopupActivation(true);
            if (popupObjects.Count > 0)
            {
                popupObjects[0].SetActive(false);
            }
            FadeIn();
            fadedIn = true;
        }
        else if((!popupColliders[0].raycasted && !popupColliders[1].raycasted) && fadedIn)
        {
            if (popupObjects.Count > 0)
            {
                popupObjects[0].SetActive(true);
            } 
            FadeOut();
            fadedIn = false;
        }
    }

    public void FadeIn()
    {
        isFadingOut = false;

        GameManager.instance.popupOpen = this; //Set the global popup currently open to this

        if (lastFadeCoroutine != null) StopCoroutine(lastFadeCoroutine);
        lastFadeCoroutine = StartCoroutine(FadeCoroutine(0f, 1f, false));
    }

    public void FadeOut()
    {
        isFadingOut = true;

        GameManager.instance.popupOpen = null; //Set the global popup open to nothing

        if (lastFadeCoroutine != null) StopCoroutine(lastFadeCoroutine);
        lastFadeCoroutine = StartCoroutine(FadeCoroutine(1f, 0f, true));
    }

    public IEnumerator FadeCoroutine(float startAlpha, float endAlpha, bool disableOnFinish)
    {
        float lerpTimer = 0f;
        float lerpTime = fadeTime;

        AudioManager.instance.Play("Popup");

        while (lerpTimer < lerpTime)
        {
            lerpTimer += Time.deltaTime;
            if (lerpTimer > lerpTime)
            {
                lerpTimer = lerpTime;
            }

            // Ease in
            float t = lerpTimer / lerpTime;
            t = Mathf.Sin(t * Mathf.PI * 0.5f);

            float alpha = Mathf.Lerp(startAlpha, endAlpha, t);

            foreach(Image thisImage in popupImages) //Update the fade of any images
            {
                Color tempColor = thisImage.color;
                tempColor.a = alpha;
                thisImage.color = tempColor;
            }
            foreach (TextMeshProUGUI text in popUpTexts) //Update the fade of any texts
            {
                Color tempColor = text.color;
                tempColor.a = alpha;
                text.color = tempColor;
            }
            yield return null;
        }

        isFadingOut = false;
        if (disableOnFinish)
        {
            PopupActivation(false);
        }
    }


    void PopupActivation(bool activate)
    {
        //Assign the objects to this activation value
        foreach (Image thisImage in popupImages) 
        {
            thisImage.gameObject.SetActive(activate);
        }
        foreach (TextMeshProUGUI text in popUpTexts)
        {
            text.gameObject.SetActive(activate);
        }
    }

}
