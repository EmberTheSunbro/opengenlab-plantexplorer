using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Menu : MonoBehaviour
{
    [SerializeField]
    GameObject playButton;

    [SerializeField]
    List<GameObject> optionButtons;

    public GameObject buttonParent;

    [SerializeField]
    GameObject moniterLogo;
    
    [SerializeField]
    TextMeshProUGUI moniterText;

    [SerializeField]
    GameObject creditsParent;

    [SerializeField]
    MicroscopeButton microscopeButton;

    [SerializeField]
    GameObject pROBE;

    [SerializeField]
    GameObject deskParent;

    
    public float deskTransitionTime = 4;

    bool isLoading = false;

    private void Start()
    {
        if (GameManager.instance.hasBeenPlayed) //if the play has started dont show play button again
        {
            foreach (GameObject button in optionButtons)
            {
                button.SetActive(true);
            }
            playButton.SetActive(false);

            ChangeTextPlay();
        }
    }

    private void Update()
    {
        if ((GameManager.instance.settingsMenu.activeSelf || creditsParent.activeSelf) && buttonParent.activeSelf)
        {
            buttonParent.SetActive(false);
        }
        else if(!GameManager.instance.settingsMenu.activeSelf && !creditsParent.activeSelf && !buttonParent.activeSelf && !isLoading)
        {
            buttonParent.SetActive(true);
        }
    }

    public void SettingsActivation()
    {
        GameManager.instance.settingsMenu.SetActive(true);
    }

    public void HasBeenPlayed()
    {
        GameManager.instance.hasBeenPlayed = true;
    }


    public void ChangeTextPlay()
    {
        ChangeTextForStage(moniterText, "<b>Microscope</b> : \n Basic synopsis of plant biology at various amplification levels.\n" +
            "<b>Thermal Cycler</b> : \n Use the provided learning materials to make your way through DNA Isolation, Amplification and Sequencing.");
    }


    public void ChangeTextForStage(TextMeshProUGUI textTobeChanged, string text)
    {
        moniterLogo.SetActive(false);
        textTobeChanged.alignment = TextAlignmentOptions.Center;
        textTobeChanged.text = text;
    }

    IEnumerator MicroScopeTransition()
    {
        isLoading = true;
        buttonParent.SetActive(false);

        //begin transition animations and de-activate/activate objects
        pROBE.GetComponent<Animator>().SetBool("isTransitioning", true);
        deskParent.GetComponent<Animator>().SetTrigger("Lower");

        AudioManager.instance.Play("MachineHum");

        yield return new WaitForSeconds(deskTransitionTime);

        AudioManager.instance.StopPlaying("MachineHum");

        GameManager.instance.sceneLoader.LoadMicroscopeScene();
    }

}
