using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleEvent : MonoBehaviour
{
    ZoomManager zoomManager;

    private void Start()
    {
        zoomManager = GameObject.FindObjectOfType<ZoomManager>();
    }

    public void PortalParticleEvent()
    {
        zoomManager.portalParticles.Play();
    }



}
