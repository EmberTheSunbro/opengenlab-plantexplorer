using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PopUpManager : MonoBehaviour
{
    [SerializeField]
    Transform cam; // Main Camera
    
    [SerializeField]
    float gazeDuration; // How long it should be gazed to trigger the action

    float timer; // Gaze timer

    [SerializeField]
    PopupCollider currentPopupCollider;
    PopupCollider oldPopupCollider;

    public void Update()
    {
        if (cam == null)
        {
            cam = GameManager.instance.gameCamera.transform;
        }

        // Raycasting to check objects for popup
        RaycastHit hit;

        if (Physics.Raycast(cam.position, cam.forward, out hit, 4000f, 1 << 5))
        {

            if (hit.collider.GetComponent<PopupCollider>() != null)
            {
                currentPopupCollider = hit.collider.GetComponent<PopupCollider>(); //Update the current collider

                if (oldPopupCollider && oldPopupCollider != currentPopupCollider) //If old popup exists and isn't the same as the new one
                {
                    oldPopupCollider.raycasted = false;
                }

                currentPopupCollider.raycasted = true;

                oldPopupCollider = currentPopupCollider;
            }
        }
        else //Nothing is being raycast
        {
            if (currentPopupCollider) //If there is currently a raycasted target
            {
                currentPopupCollider.raycasted = false;
                currentPopupCollider = null;
            }
            if (oldPopupCollider)
            {
                oldPopupCollider.raycasted = false;
                oldPopupCollider = null;
            }
        }
    }
}
