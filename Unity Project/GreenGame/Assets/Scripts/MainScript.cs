﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainScript : MonoBehaviour {

	public GameObject sphere;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (Application.platform == RuntimePlatform.Android) {
			if (Input.GetKeyUp(KeyCode.Escape)) {
				//quit application on return button
				Application.Quit();
				return;
			}
		}
	}

	void OnEnable() {
		MagneticClickKludger.onMagneticTriggerPress += onMagneticTriggerPress;
		MagneticClickKludger.onMagneticTriggerRelease += onMagneticTriggerRelease;
		MagneticClickKludger.onMagneticTriggerAnomaly += onMagneticTriggerAnomaly;
		sphere.GetComponent<Renderer>().material.SetColor("_Color", Color.cyan);
	}

	void OnDisable() {
		MagneticClickKludger.onMagneticTriggerPress -= onMagneticTriggerPress;
		MagneticClickKludger.onMagneticTriggerRelease -= onMagneticTriggerRelease;
		MagneticClickKludger.onMagneticTriggerAnomaly -= onMagneticTriggerAnomaly;
	}

	public void onMagneticTriggerPress() {
		sphere.GetComponent<Renderer>().material.SetColor("_Color", Color.red);
	}

	public void onMagneticTriggerRelease() {
		sphere.GetComponent<Renderer>().material.SetColor("_Color", Color.green);
	}

	public void onMagneticTriggerAnomaly(float dx, float dy) {
		sphere.GetComponent<Renderer>().material.SetColor("_Color", Color.magenta);
		// I used $" string interpolation in the Debug.Log, which requires dotnet 4.x.
		// Player Settings -> Other Settings -> Configuration -> Scripting Runtime Version -> dotNET 4.x Equivalent  
		// Commented-out to avoid causing needless headaches if you're still using dotNET 3.5
		// Debug.Log($"Magnetic trigger anomaly: dx={dx}, dy={dy}");
	}
}
