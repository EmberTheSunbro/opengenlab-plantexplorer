using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagneticClickKludger : MonoBehaviour
{
	public delegate void MagneticTriggerPress();
	public delegate void MagneticTriggerRelease();
	public delegate void MagneticTriggerAnomaly(float dx, float dy);
	public static event MagneticTriggerPress onMagneticTriggerPress;
	public static event MagneticTriggerRelease onMagneticTriggerRelease;
	public static event MagneticTriggerAnomaly onMagneticTriggerAnomaly;

	private Vector3[] buffer = new Vector3[20];
	private int bufferIndex = -1;

	public bool isMagButtonEnabled = true;
	public bool isGamepadEnabled = true;
	public bool isCapacitiveEnabled = true;
	public string buttonInputName = "btnAny";

	void OnEnable()
	{
		// magnetometer is disabled by default to save power
		if (isMagButtonEnabled)
		{
			Input.compass.enabled = true;
			Input.gyro.enabled = true;
		}
	}

	void OnDisable()
	{
		// turn off magnetometer when we aren't using it to save power.
		if (isMagButtonEnabled)
		{
			Input.compass.enabled = false;
			Input.gyro.enabled = false;
		}
	}

	// Use this for initialization
	void Start()
	{
		// pre-fill buffer with current sensor values, just so we'll have something to work with.
		Vector3 raw = Input.compass.rawVector;
		for (int x = 0; x < buffer.Length; x++)
			buffer[x] = raw;
	}

	// Update is called once per frame
	void Update()
	{
		if (isMagButtonEnabled)
			analyzeMagButton();

		if (isGamepadEnabled)
			analyzeGamepad();

		// Skip unless we're running on actual Android hardware, as opposed to within the Unity Editor 
		// (throws NPE if executes under Windows via GvrEditorEmulator)
		if (Application.platform == RuntimePlatform.Android && isCapacitiveEnabled)
			analyzeTouches();
	}

	// Detect taps from Cardboard2.0-type headpiece
	private void analyzeTouches()
	{
		if (Input.touchCount == 0)
			return;
		Touch firstTouch = Input.GetTouch(0);
		Touch lastTouch = Input.GetTouch(Input.touchCount - 1);
		if (firstTouch.phase == TouchPhase.Began)
			onMagneticTriggerPress();
		else if (lastTouch.phase == TouchPhase.Ended)
			onMagneticTriggerRelease();
		else if (lastTouch.phase == TouchPhase.Began)
			onMagneticTriggerPress();
		else if (firstTouch.phase == TouchPhase.Ended)
			onMagneticTriggerRelease();
	}

	// Detect button press on gamepad.
	
	private void analyzeGamepad()
	{
		if (Input.GetButtonDown(buttonInputName))
			onMagneticTriggerPress();
		else if (Input.GetButtonUp(buttonInputName))
			onMagneticTriggerRelease();
	}

	private void analyzeMagButton()
	{
		bufferIndex = (bufferIndex + 1) % 10;
		Vector3 raw = Input.compass.rawVector;

		Vector3 gyro = Input.gyro.rotationRate;
		// we reset the buffer whenever there's significant diagonal motion
		if ((Mathf.Abs(gyro.x) > 0.05) && (Mathf.Abs(gyro.y) > 0.05f))
		{
			for (int x = 0; x < 20; x++)
				buffer[x] = raw;
		}

		else
		{
			// We save two copies in the array because it's faster than complex circular-buffer logic or shifting everything each time.
			buffer[bufferIndex] = raw;
			buffer[bufferIndex + 10] = raw;

			evaluate();
		}
	}

	private void evaluate()
	{
		int i = 1;
		float dx, dy;
		do
		{
			dx = buffer[bufferIndex + i].x - buffer[bufferIndex].x;
			dy = buffer[bufferIndex + i].y - buffer[bufferIndex].y;

			// consider it triggered ONLY if BOTH dx AND dy are > 5, and dx & dy are at least somewhat close in magnitude
			if ((Mathf.Abs(dx) > 5) && (Mathf.Abs(dy) > 5) && (Mathf.Abs(dx - dy) < 3))
			{
				Vector3 lastValue = buffer[bufferIndex];
				// clear the buffer by setting everything in it to the current value
				for (int x = 0; x < 20; x++)
					buffer[x] = lastValue;

				// don't call event unless it's non-null & therefore has at least one subscriber.
				if ((dx > 0) && (dy > 0) && (onMagneticTriggerRelease != null))
					onMagneticTriggerRelease();
				else if ((dx < 0) && (dy < 0) && (onMagneticTriggerPress != null))
					onMagneticTriggerPress();
				else if (onMagneticTriggerAnomaly != null)
					onMagneticTriggerAnomaly(dx, dy);

				return;
			}
		}
		while (++i < 10);
	}
}
