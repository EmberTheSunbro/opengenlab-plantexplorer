using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CoroutineManager : MonoBehaviour
{
    private void Start()
    {
        
    }


    public void BeginWaitCoroutine(Action endAction, float delay)
    {
        StartCoroutine(WaitCoroutine(endAction, delay));
    }

    public IEnumerator WaitCoroutine(Action endAction, float delay)
    {
        yield return new WaitForSeconds(delay);
        endAction();
    }
}
