using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SettingsManager : MonoBehaviour
{
    Transform cam; // Main Camera

    [SerializeField]
    float gazeDuration; // How long it should take to trigger the action

    float timer; //Gaze time

    [SerializeField]
    GameObject settingsPopup;

    //public string currentLayer = "Settings";

    public AudioListener audioListener;

    // Start is called before the first frame update
    void Start()
    {
  
    }

    // Update is called once per frame
    void Update()
    {
        //Raycasting to check objects for popup
        RaycastHit hit;

        if(cam == null && GameManager.instance.gameCamera != null )
        {
            cam = GameManager.instance.gameCamera.transform;
        }
        else if(cam != null)
        {
            if (Physics.Raycast(cam.position, cam.forward, out hit, 100000f))
            {
                SettingsInfo settingsInfoScript = hit.collider.GetComponent<SettingsInfo>();
                if (settingsInfoScript != null)
                {
                    timer += Time.deltaTime;

                    // If player gazes for a certain amount of time
                    if (timer >= gazeDuration)
                    {
                        settingsInfoScript.ShowSettingsMenu(true);
                    }

                }
                else
                {
                    timer = 0f;
                }
            }
            else
            {
                timer = 0f;
            }
        }
    }

    public void SetVolume(float newVolume)
    {
        AudioListener.volume = newVolume;
    }

    public void Exit()
    {
        settingsPopup.SetActive(false);
    }
}

