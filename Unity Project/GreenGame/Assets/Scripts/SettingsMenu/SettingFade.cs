using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class SettingFade : MonoBehaviour
{
    [System.NonSerialized] public bool isFadingOut = false; // prevents fading out again if already fading out

    Coroutine lastFadeCoroutine;

    private float fadeTime = 1f;

    public GameObject settingsPopUpUI;
    Image panel;
    float panelRef;

    Color startColor;

    //public List<Component> stuffToDisable;
    //public List<Component> stuffToEnable;

    private void Awake()
    {
        panel = settingsPopUpUI.GetComponentInChildren<Image>();
        startColor = panel.color; 
        panelRef = panel.color.a;
    }

    public void FadeIn()
    {
        isFadingOut = false;

        if (lastFadeCoroutine != null) StopCoroutine(lastFadeCoroutine);
        lastFadeCoroutine = StartCoroutine(FadeCoroutine(0f, 1f, false));
    }

    public void FadeOut()
    {
        isFadingOut = true;

        if (lastFadeCoroutine != null) StopCoroutine(lastFadeCoroutine);
        lastFadeCoroutine = StartCoroutine(FadeCoroutine(1f, 0f, true));
    }

    public IEnumerator FadeCoroutine(float startAlpha, float endAlpha, bool disableOnFinish)
    {
        float lerpTimer = 0f;
        float lerpTime = fadeTime;

        while (lerpTimer < lerpTime)
        {
            lerpTimer += Time.deltaTime;
            if (lerpTimer > lerpTime)
            {
                lerpTimer = lerpTime;
            }

            // Ease in
            float t = lerpTimer / lerpTime;
            t = Mathf.Sin(t * Mathf.PI * 0.5f);

            float alpha = Mathf.Lerp(startAlpha, endAlpha, t);
            panel.color = new Color(startColor.r, startColor.g, startColor.b, alpha * panelRef);
            //popUpText.color = new Color(1f, 1f, 1f, alpha);
            yield return null;
        }

        isFadingOut = false;
        //if (disableOnFinish)
        //{
        //    settingsPopUpUI.SetActive(false);
        //    GetComponent<SettingsInfo>().SetFalseSettingsShowInfo();
        //}
    }
}
