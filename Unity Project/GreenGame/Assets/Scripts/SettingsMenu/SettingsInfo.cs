using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class SettingsInfo : MonoBehaviour
{
    [SerializeField]
    GameObject settingsPopup;

    private Coroutine lastCoroutine;

    private bool isShowingSettingsInfo = false;

    private void Start()
    {
        //thisCellPopup = gameObject.GetComponent<PopUpFade>().popUpUI;
    }

    public void ShowSettingsMenu(bool enabled)
    {
        // Only called when players are looking at the cell part
        settingsPopup.SetActive(enabled);

        if (lastCoroutine != null)
        {
            StopCoroutine(lastCoroutine);
        }

        if (!isShowingSettingsInfo)
        {
            //Debug.Log("Fading");
            if (enabled) GetComponent<SettingFade>().FadeIn();
            isShowingSettingsInfo = true;
        }

        lastCoroutine = StartCoroutine(WaitCoroutine());
    }

    private IEnumerator WaitCoroutine()
    {
        yield return new WaitForSeconds(0.5f);
        //GetComponent<SettingFade>().FadeOut();
    }

    public void SetFalseSettingsShowInfo()
    {
        isShowingSettingsInfo = false;
    }
}
