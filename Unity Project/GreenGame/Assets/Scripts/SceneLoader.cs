using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoader : MonoBehaviour
{
    public Animator transition;

    // Booleans checks for loading scenes
    public bool isLoadingMicroscopeScene = false;
    public bool isLoadingSequencerScene = false;
    public bool isLoadingThermalCyclerScene = false;

    public delegate void SceneTransfer(string sceneName);
    public static event SceneTransfer OnSceneTransfer;
    
    public delegate void EarlySceneTransfer(string sceneName);
    public static event EarlySceneTransfer OnEarlySceneTransfer;

    public void LoadMainScene()
    {
        //GameManager.instance.mainLightSettings.bakedGI = true;
        AudioManager.instance.Play("LabAmbience01");
        AudioManager.instance.StopPlaying("Wind");
        AudioManager.instance.StopPlaying("LabAmbience02");
        AudioManager.instance.StopPlaying("MMusic1");
        StartCoroutine(WaitCoroutine("MainScene"));
    }

    public void LoadMicroscopeScene()
    {
        AudioManager.instance.StopPlaying("LabAmbience01");
        AudioManager.instance.StopPlaying("LabAmbience02");
        isLoadingMicroscopeScene = true;       
        AudioManager.instance.Play("Wind");
        AudioManager.instance.Play("MMusic1");

        StartCoroutine(WaitCoroutine("Microscope"));
    }

    public void LoadSequencerScene()
    {
        isLoadingSequencerScene = true;
        StartCoroutine(WaitCoroutine("Sequencer"));
    }

    public void LoadThermalCyclerScene()
    {
        AudioManager.instance.StopPlaying("LabAmbience01");
        AudioManager.instance.StopPlaying("Wind");
        AudioManager.instance.Play("LabAmbience02");
        isLoadingThermalCyclerScene = true;
        StartCoroutine(WaitCoroutine("ThermalCycler"));
        GameManager.instance.checkpointManager.DisableCheckpointButtons();
    }

    IEnumerator WaitCoroutine(string sceneName)
    {
        AudioManager.instance.Play("Scene");

        AudioManager.instance.StopPlaying("Fill");

        //SceneLoader.OnEarlySceneTransfer(sceneName);
        while (!GameManager.instance.screenBlackedOut) //Until the screen is black and ready to be moved
        {
            GameManager.instance.blackScreenToggle = true;
            
            yield return null;
        }

        GameManager.instance.popupOpen = null; //Nullify any opened popups 
        
        GameManager.instance.gameCamera.GetComponent<Camera>().cullingMask = GameManager.instance.startMask; //Reset the mask to the starting layermask
        
        SceneManager.LoadScene(sceneName);

        SceneLoader.OnSceneTransfer(sceneName);
        
        GameManager.instance.macroToggle.isOn = true; //Set it to the macrotoggle when loading new scenes to make sure the zooming always starts properly

        GameManager.instance.blackScreenToggle = false;

        //InventoryManager.instance.ResetReferences();
    }

}
