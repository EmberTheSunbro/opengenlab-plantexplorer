// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/InputMaster.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @InputMaster : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @InputMaster()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputMaster"",
    ""maps"": [
        {
            ""name"": ""DesktopPlayer"",
            ""id"": ""0f6e445b-4d8a-4a9b-b0d2-27d6b0283e00"",
            ""actions"": [
                {
                    ""name"": ""Look"",
                    ""type"": ""PassThrough"",
                    ""id"": ""ede90c69-9c0f-4e7a-9d41-4502e873caeb"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""7ea59d4b-1496-47a4-8208-445802c9e2a4"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // DesktopPlayer
        m_DesktopPlayer = asset.FindActionMap("DesktopPlayer", throwIfNotFound: true);
        m_DesktopPlayer_Look = m_DesktopPlayer.FindAction("Look", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // DesktopPlayer
    private readonly InputActionMap m_DesktopPlayer;
    private IDesktopPlayerActions m_DesktopPlayerActionsCallbackInterface;
    private readonly InputAction m_DesktopPlayer_Look;
    public struct DesktopPlayerActions
    {
        private @InputMaster m_Wrapper;
        public DesktopPlayerActions(@InputMaster wrapper) { m_Wrapper = wrapper; }
        public InputAction @Look => m_Wrapper.m_DesktopPlayer_Look;
        public InputActionMap Get() { return m_Wrapper.m_DesktopPlayer; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(DesktopPlayerActions set) { return set.Get(); }
        public void SetCallbacks(IDesktopPlayerActions instance)
        {
            if (m_Wrapper.m_DesktopPlayerActionsCallbackInterface != null)
            {
                @Look.started -= m_Wrapper.m_DesktopPlayerActionsCallbackInterface.OnLook;
                @Look.performed -= m_Wrapper.m_DesktopPlayerActionsCallbackInterface.OnLook;
                @Look.canceled -= m_Wrapper.m_DesktopPlayerActionsCallbackInterface.OnLook;
            }
            m_Wrapper.m_DesktopPlayerActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Look.started += instance.OnLook;
                @Look.performed += instance.OnLook;
                @Look.canceled += instance.OnLook;
            }
        }
    }
    public DesktopPlayerActions @DesktopPlayer => new DesktopPlayerActions(this);
    public interface IDesktopPlayerActions
    {
        void OnLook(InputAction.CallbackContext context);
    }
}
