using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class SlotManager : MonoBehaviour
{
    public PuzzleSystemExtraction puzzleSystemExtraction;

    public PuzzleSlot[] puzzleSlots;

    public GameObject logo;

    [SerializeField]
    GameObject wrongMatPortal;

    [SerializeField]
    GameObject rightMatPortal;

    [SerializeField]
    private PuzzleSlot[] rightPuzzleSlots;

    private PuzzleSlot selectedSlot1;
    private PuzzleSlot selectedSlot2;

    public Ingredient[] ingredientsToPlace;

    public GameObject[] ingredientButtons;

    [SerializeField]
    private VerifyBtn verifyBtn;

    [SerializeField]
    RemoveSupernatantBtn removeSupernatantBtn;

    [SerializeField]
    RunThermalCycler runThermalCycler;

    [SerializeField]
    ProceedSeqBtn seqBtn;

    //[SerializeField]
    //private GameObject SelectPlantButton;

    [SerializeField]
    private GameObject verifyButton;

    [SerializeField]
    GameObject[] extractionTools;

    [SerializeField]
    GameObject[] firstExtractionbutton;

    [SerializeField]
    GameObject[] plantSamples;

    [SerializeField]
    Transform monitorStart;

    [SerializeField]
    Transform monitorEnd;

    [SerializeField]
    GameObject pcrButton;

    [SerializeField]
    GameObject centrifuge;

    [SerializeField]
    GameObject pipette;

    [SerializeField]
    CoroutineManager coroutineManager;

    Vector3 originalMonitorPos;

    Vector3 monitorPlayPosition;

    public bool isSortingSuccessful;
    private bool isTrailRotating = false;

    public bool checkpoint1 = false; // Organizing tools
    public bool checkpoint2 = false; // Buffer stage
    public bool checkpoint3 = false; // Primers PCR stage
    public bool checkpoint4 = false; // Primers Seq stage

    CheckpointManager cm;

    public TextMeshProUGUI textMeshPro;

    [SerializeField]
    GameObject trail;

   [SerializeField]
   private Transform trailTargetRotation;

    [SerializeField]
    GameObject notepad;

    public GameObject attentionRing;

    private void Awake()
    {     

    }

    public void Start()
    {
        //HideIngredientButtons();
        cm = GameManager.instance.checkpointManager;
        if(cm.toolsOut == true)
        {
            cm.toolsOut = false;
            coroutineManager.BeginWaitCoroutine(() =>
            {
                MoveToolsToSlots();
            }, 2f);
        }
        CheckpointActivationChecker(); // Called to reload scene based on checkpoint activated
        originalMonitorPos = monitorStart.transform.position;     
        RandomizeIngredients(); // Randomizing ingredients at the start
        ChangeTextForStage(textMeshPro, "Place the items in correct order of execution. Starting from left side of the table to the right.");
    }

    private void Update()
    {
        if(CheckIngredientSlots() == true)
        {
            verifyButton.gameObject.SetActive(true);
            attentionRing.SetActive(true);
            StartCoroutine(MonitorLerpCoroutine(monitorStart, monitorEnd.transform.position));
        }
        else
        {
            verifyButton.gameObject.SetActive(false);
        }
        if (isTrailRotating && !isSortingSuccessful)
        {
            TrailRotation();
        }
        else
        {

        }
    }

    // Function to put ingredients in random order
    void RandomizeIngredients()
    {
        for (int j = 0; j < rightPuzzleSlots.Length; j++)
        {
            rightPuzzleSlots[j].SetStoredIngredient(null);
        }

        int currentIndexValue = 0;

        List<Ingredient> ingredients = new List<Ingredient>(ingredientsToPlace);
        //Debug.Log(ingredients.Count + " Med ");

        while(ingredients.Count > 0)
        {
            int rng = Random.Range(0, ingredients.Count);            
            MoveIngredientToEmptySlot(rng);
            ingredients.RemoveAt(rng);           
            //SettingPuzzleSlotsNull();
        }
      
        // Move ingredients to empty slots
        void MoveIngredientToEmptySlot(int index)
        {
            //Debug.Log(rightPuzzleSlots[currentIndexValue].storedIngredient);
            if (rightPuzzleSlots[currentIndexValue].storedIngredient == null)
            {
                //StartCoroutine(LerpCoroutine(ingredients[index], rightPuzzleSlots[currentIndexValue]));  
                          
                rightPuzzleSlots[currentIndexValue].SetStoredIngredient(ingredients[index]);
                //HideSortingTools();
                ingredients[index].transform.position = rightPuzzleSlots[currentIndexValue].transform.position;               
                currentIndexValue += 1;
            }
            
        }
    }

    public void ShowIngredientButtons()
    {
        for(int i = 0; i< ingredientButtons.Length; i++)
        {
            ingredientButtons[i].SetActive(true);
        }
    }

    public void HideIngredientButtons()
    {
        for (int i = 0; i < ingredientButtons.Length; i++)
        {
            ingredientButtons[i].SetActive(false);
        }
    }

    // Function called by "Verify Order" button and checks if the order is right
    void VerifyIngredientOrder()
    {
        if(puzzleSlots[0].storedIngredient.ingredientType == PuzzleSystemExtraction.IngredientType.MortarPestle
           && puzzleSlots[1].storedIngredient.ingredientType == PuzzleSystemExtraction.IngredientType.Buffer
           && puzzleSlots[2].storedIngredient.ingredientType == PuzzleSystemExtraction.IngredientType.Centrifuge
           && puzzleSlots[3].storedIngredient.ingredientType == PuzzleSystemExtraction.IngredientType.Supernatant)

        {
            // Verifies true
            //Debug.Log("Success");
            attentionRing.SetActive(false);
            AudioManager.instance.Play("Correct");
            ChangeTextForStage(textMeshPro, "Correct!");
            checkpoint1 = true;
            isSortingSuccessful = true;
            cm.toolsOrganized = true;
            isTrailRotating = false;
            //cm.currentCheckpoint = 1;
            verifyBtn.HideIngredientButtons();
            verifyBtn.ShowExtractionButtons();
            SettingPuzzleSlotsNull();
            pipette.SetActive(true);
            centrifuge.GetComponent<Animator>().enabled = true;
            notepad.SetActive(false);
            coroutineManager.BeginWaitCoroutine(() =>
            {               
                ChangeTextForStage(textMeshPro, "");
                logo.SetActive(true);
            }, 6f);
            //ShowExtractionIngredientButtons();
        }
        else
        {
            //Randomize - IF WRONG
            //Debug.Log("Failure");
            attentionRing.SetActive(false);
            AudioManager.instance.Play("Incorrect");
            ChangeTextForStage(textMeshPro, "Incorrect order. Let's try again.");
            isSortingSuccessful = false;
            isTrailRotating = true;
            StartCoroutine(MonitorLerpCoroutine(monitorEnd, originalMonitorPos));
            RandomizeIngredientWithEffects();
            HideIngredientButtons();
            SettingPuzzleSlotsNull();
            StartCoroutine(PushOutWaitCoroutine());
            coroutineManager.BeginWaitCoroutine(() =>
            {
                ChangeTextForStage(textMeshPro, "");
                logo.SetActive(true);
            }, 6f);
            //coroutineManager.BeginWaitCoroutine(() =>
            //{
            //    ShowSortingTools();
            //}, 2f);

            coroutineManager.BeginWaitCoroutine(() =>
            {
                ShowIngredientButtons();
            }, 3.5f);            
        }
    }

    bool CheckIngredientSlots()
    {
        for (int i = 0; i < puzzleSlots.Length; i++)
        {
            if(puzzleSlots[i].storedIngredient == null)
            {
                return false;
            }
        }
        return true;
    }

    //Function called by the button to verify ingredients
    public void HandleSlotsFull()
    {
        if (CheckIngredientSlots() == true)
        {
            VerifyIngredientOrder();
        }
    }

    // Setting the slots to null 
    void SettingPuzzleSlotsNull()
    {
        for(int j = 0; j < puzzleSlots.Length; j++)
        {
            puzzleSlots[j].SetStoredIngredient(null);
        }
        
    }

    // Coroutine for moving back tools
    public IEnumerator LerpCoroutine(Ingredient ingredientToMove, PuzzleSlot destinationSlot)
    {
        //Debug.Log(ingredientToMove.puzzleSlot);
        Vector3 startPos = ingredientToMove.puzzleSlot.transform.position;
        Vector3 endPos = destinationSlot.transform.position;

        float lerpTimer = 0f;
        float lerpTime = 5f;


        while (lerpTimer < lerpTime)
        {            
            lerpTimer += Time.deltaTime;
            if (lerpTimer > lerpTime)
            {
                lerpTimer = lerpTime;
            }

            // Lerp math
            float t = lerpTimer / lerpTime;
            t = Mathf.Sin(t * Mathf.PI * 0.5f);

            ingredientToMove.transform.position = Vector3.Lerp(startPos, endPos, t);
            //Debug.Log(ingredientToMove.transform.position);
            yield return null;
        }
    }

    // Lerp coroutine for monitors
    public IEnumerator MonitorLerpCoroutine(Transform monitorStartPos, Vector3 monitorEndPos)
    {
        Vector3 startPos = monitorStartPos.transform.position;
        Vector3 endPos = monitorEndPos;

        float lerpTimer = 0f;
        float lerpTime = 5f;


        while (lerpTimer < lerpTime)
        {
            lerpTimer += Time.deltaTime;
            if (lerpTimer > lerpTime)
            {
                lerpTimer = lerpTime;
            }

            // Lerp math
            float t = lerpTimer / lerpTime;
            t = Mathf.Sin(t * Mathf.PI * 0.5f);

            monitorStart.transform.position = Vector3.Lerp(startPos, endPos, t);
            //Debug.Log(ingredientToMove.transform.position);
            yield return null;
        }
    }

    // Gateway portal effects for pushing out
    void ShowIngredientGameObjects()
    {
        coroutineManager.BeginWaitCoroutine(() =>
        {
            for (int i = 0; i < extractionTools.Length; i++)
            {
                GatewayPortalManager portalManager = extractionTools[i].GetComponent<GatewayPortalManager>();               
                //currentTools[i].SetActive(false);
                portalManager.PushOut();
            }
        }, 2f);
       
    }

    // Gateway portal effects for pulling in
    void HideSortingTools()
    {       
        for (int i = 0; i < 4; i++)
        {
            GatewayPortalManager portalManager = extractionTools[i].GetComponent<GatewayPortalManager>(); 
            //Switching materials
            portalManager.gateway = wrongMatPortal;           
            //currentTools[i].SetActive(false);
            portalManager.PullIn();
        }
    }

    // Gateway portal effects for pushing out
    void ShowSortingTools()
    {
        for (int i = 0; i < 4; i++)
        {
            GatewayPortalManager portalManager = extractionTools[i].GetComponent<GatewayPortalManager>();
            portalManager.gateway = rightMatPortal;
            //currentTools[i].SetActive(false);
            portalManager.PushOut();           
        }

    }

    void FirstSetExtractionBtns()
    {
        for (int i = 0; i < firstExtractionbutton.Length; i++)
        {
            firstExtractionbutton[i].SetActive(true);
        }
    }

    void HideFirstSetExtractionBtns()
    {
        for (int i = 0; i < firstExtractionbutton.Length; i++)
        {
            firstExtractionbutton[i].SetActive(false);
        }
    }

    void ShowPlantSamples()
    {
        for (int i = 0; i < plantSamples.Length; i++)
        {
            plantSamples[i].SetActive(true);
        }
    }

    public void PutOrganizationToolsBack()
    {
        // First checpoint       
        // Enables orgranized tools and buttons
        //verifyBtn.HideIngredientButtons();
        ShowIngredientGameObjects();
        //ShowIngredientButtons();
        FirstSetExtractionBtns();
        ShowPlantSamples();       
        verifyBtn.gameObject.SetActive(false);
    }

    public void PutPCRToolsBack()
    {
        // Second checpoint       
        // Enables pcr buttons
        pcrButton.SetActive(true);
    }

    void RandomizeIngredientWithEffects()
    {
        for (int j = 0; j < rightPuzzleSlots.Length; j++)
        {
            rightPuzzleSlots[j].SetStoredIngredient(null);
        }

        int currentIndexValue = 0;

        List<Ingredient> ingredients = new List<Ingredient>(ingredientsToPlace);
        //Debug.Log(ingredients.Count + " Med ");

        while (ingredients.Count > 0)
        {
            int rng = Random.Range(0, ingredients.Count);
            MoveIngredientToEmptySlot(rng);
            ingredients.RemoveAt(rng);
            //SettingPuzzleSlotsNull();
        }

        void MoveIngredientToEmptySlot(int index)
        {
            //Debug.Log(rightPuzzleSlots[currentIndexValue].storedIngredient);
            if (rightPuzzleSlots[currentIndexValue].storedIngredient == null)
            {              
                rightPuzzleSlots[currentIndexValue].SetStoredIngredient(ingredients[index]);
                HideSortingTools();
                ingredients[index].transform.position = rightPuzzleSlots[currentIndexValue].transform.position;
                //Debug.Log(ingredients[index].transform.position);
                currentIndexValue += 1;
                HideSortingTools();
            }

        }
    }

    private IEnumerator PushOutWaitCoroutine()
    {
        yield return new WaitForSeconds(2f);
        ShowSortingTools();
    }

    public void MoveToolsToSlots()
    {
        // Use this function for checkpoint toggles 
        extractionTools[0].transform.position = puzzleSlots[1].transform.position; // Buffer to Slot 2
        extractionTools[1].transform.position = puzzleSlots[3].transform.position; // Supernatant to Slot 4
        extractionTools[2].transform.position = puzzleSlots[2].transform.position; // Centrifuge to Slot 3
        extractionTools[3].transform.position = puzzleSlots[0].transform.position; // Mortar and Pestle to Slot 1       
    }


    void CheckpointActivationChecker()
    {
        if (cm.currentlySelectedCheckpoint == 3)
        {
            ToggleSystemCheckpointThree();
        }
        else if (cm.currentlySelectedCheckpoint == 2)
        {
            ToggleSystemCheckpointTwo();
        }
        else if (cm.currentlySelectedCheckpoint == 1)
        {
            ToggleSystemCheckpointOne();
        }
    }

    void ToggleSystemCheckpointOne()
    {
        // Reload Scene and enable checkpoint 
        // Plant selection step after sorting
        Debug.Log("Checkpoint 1");
        //cm.currentCheckpoint = 1;
        //MoveToolsToSlots();
        //verifyBtn.gameObject.SetActive(false);      
        coroutineManager.BeginWaitCoroutine(() =>
        {
            MoveToolsToSlots();
            //verifyBtn.HideIngredientButtons();
            //verifyBtn.ShowExtractionButtons();
            //pipette.SetActive(true);
            //centrifuge.GetComponent<Animator>().enabled = true;
        }, 2f);     
    }

    void ToggleSystemCheckpointTwo()
    {
        // Maybe reload scene fade in and fade out and do this
        // "Select and Add DNA" step after BUFFER step is correct
       //cm.currentCheckpoint = 2;
        ToggleSystemCheckpointOne();
        HideFirstSetExtractionBtns();
        coroutineManager.BeginWaitCoroutine(() =>
        {
            removeSupernatantBtn.HideExtractionTools();
            removeSupernatantBtn.PushOutNextTools();
            removeSupernatantBtn.nextToolBtn.SetActive(true);
        }, 2f);      
    }

    void ToggleSystemCheckpointThree()
    {
        // Sequencing Stage after PRIMER is right
        //cm.currentCheckpoint = 3;
        ToggleSystemCheckpointTwo();
        removeSupernatantBtn.nextToolBtn.SetActive(false);
       
        coroutineManager.BeginWaitCoroutine(() =>
        {
            runThermalCycler.HidePCRObjects();
            pcrButton.SetActive(false);
            seqBtn.ShowSequencingTools();
            seqBtn.nextSeqBtn.SetActive(true);
        }, 6f);
    }

    // Trail moving from the left to the right side of the table
    void TrailRotation()
    {
        trail.SetActive(true);
        float speed = 1f;
        trail.transform.rotation = Quaternion.Lerp(trail.transform.rotation, trailTargetRotation.rotation, Time.deltaTime * speed);

        if(trail.transform.rotation == trailTargetRotation.rotation)
        {
            trail.SetActive(false);
            isTrailRotating = false;
            trail.transform.rotation = Quaternion.Euler(0, 0, 0);       
        }
    }

    // Universal function to change text on Monitor whenever needed
    public void ChangeTextForStage(TextMeshProUGUI textToBeChanged, string text)
    {
        logo.SetActive(false);
        textToBeChanged.alignment = TextAlignmentOptions.Center;
        textToBeChanged.text = text;
    }
}
