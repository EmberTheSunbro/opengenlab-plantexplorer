using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTransition : MonoBehaviour
{
    [SerializeField]
    private Transform[] transitionPoints;

    [SerializeField]
    private float transitionSpeed;

    private Camera cam;

    Transform currentposition;


    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
    }


    void LateUpdate()
    {
        if(currentposition == null)
        {
            return;
        }

        // Lerping between two points
        cam.transform.position = Vector3.Lerp(cam.transform.position, currentposition.position, Time.deltaTime * transitionSpeed);

        // Smooth rotation between points if needed
        Vector3 currentAngle = new Vector3(
                               Mathf.LerpAngle(cam.transform.rotation.eulerAngles.x, currentposition.transform.rotation.eulerAngles.x, Time.deltaTime * transitionSpeed),
                               Mathf.LerpAngle(cam.transform.rotation.eulerAngles.y, currentposition.transform.rotation.eulerAngles.y, Time.deltaTime * transitionSpeed),
                               Mathf.LerpAngle(cam.transform.rotation.eulerAngles.z, currentposition.transform.rotation.eulerAngles.z, Time.deltaTime * transitionSpeed));

        cam.transform.eulerAngles = currentAngle;

    }

    public void StartToMainSceneTransition()
    {
        // Lerps to lab scene
        currentposition = transitionPoints[0];
    }

    public void MainToStartSceneTransition()
    {
        //Lerps back to main scene
        currentposition = transitionPoints[1];
    }

}
