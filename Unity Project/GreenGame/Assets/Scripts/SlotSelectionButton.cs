using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SlotSelectionButton : SpecialSelectionButton
    //, IPointerClickHandler
{
    [SerializeField]
    private SlotManager slotManager;

    public Ingredient ingredientType;

    public override void PointerClick()
    {
        base.PointerClick();
        InventoryManager.instance.PickUpIngredient(ingredientType);
        ingredientType.DisableButton();        
        highlightedImage.fillAmount = 0;
        //slotManager.SelectSlot(ingredientType.puzzleSlot);
    }
}
