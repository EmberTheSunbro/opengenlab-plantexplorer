using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChanger : MonoBehaviour
{
    [SerializeField] private Color myColor;
    private Renderer myRenderer;

    // Start is called before the first frame update
    void Awake()
    {
        myRenderer = gameObject.GetComponent<Renderer>();
        myRenderer.material.color = myColor;
    }


    public void ApplyColor(Color recievedColor)
    {
        myColor = recievedColor;
        myRenderer.material.color = myColor;
    }

    void Green()
    {
        myColor = Color.green;
        myRenderer.material.color = myColor;
    }

    void Red()
    {
        myColor = Color.red;
        myRenderer.material.color = myColor;
    }

    void Blue()
    {
        myColor = Color.blue;
        myRenderer.material.color = myColor;
    }
}
