using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SpecialCheckpointButton : SpecialSelectionButton
{
    //Assign text to appear during the onpointer enter
    public Animator hoverPanelTextToActivate;

    bool currentlySelected = false; //if set true then this needs to become not selectable and also stay at alpha 1

    CheckpointManager cm;

    public int thisCheckpoint;

    protected override void Awake()
    {
        base.Awake();
        
    }
    private void Start()
    {
        cm = GameManager.instance.checkpointManager;
    }
    private void Update()
    {
        if (SceneManager.GetActiveScene().name == "ThermalCycler")
        {
            if (cm.currentlySelectedCheckpoint == thisCheckpoint && !currentlySelected)
            {
                currentlySelected = true;

                Color tempColor = highlightedImage.color;
                tempColor.a = 1;
                highlightedImage.color = tempColor;
            }
            else if (cm.currentlySelectedCheckpoint != thisCheckpoint && currentlySelected)
            {
                Color tempColor = highlightedImage.color;
                tempColor.a = 0;
                highlightedImage.color = tempColor;

                currentlySelected = false;
            }
        }
    }

    public override void PointerEnter()
    {
        hoverPanelTextToActivate.SetTrigger("Appear");
        if (!currentlySelected)
        {
            base.PointerEnter();
        }
    }

    public override void PointerExit()
    {
        hoverPanelTextToActivate.SetTrigger("Disappear");
        if (!currentlySelected)
        {
            base.PointerExit();
        }
    }

    public override void PointerClick()
    {
        base.PointerClick();
    }
}
