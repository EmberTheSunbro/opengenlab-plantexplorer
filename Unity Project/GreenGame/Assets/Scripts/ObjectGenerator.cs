using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectGenerator : MonoBehaviour
{
    public int objectCount;

    public GameObject objectPrefab;
    public GameObject objectHolder;

    public float spaceBetweenPrefabs = 5;

    float prefabSpaceCounter;

    public Text counterText;

    public Text frames;

    private void Update()
    {
        frames.text = "Frames: " + (1f / Time.deltaTime).ToString();

        if(counterText.text != "counter: " + objectCount.ToString())
        {
            counterText.text = "counter: " + objectCount.ToString();
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            objectCount++;
            prefabSpaceCounter += spaceBetweenPrefabs;
            Instantiate(objectPrefab, new Vector3(0, prefabSpaceCounter, 0), Quaternion.identity, objectHolder.transform);
        }

        if(Input.touchCount > 1 || Input.GetKeyDown(KeyCode.X))
        {
            objectCount = 0;
            prefabSpaceCounter = 0;
            foreach (Transform child in objectHolder.transform)
            {
                Destroy(child.gameObject);
            }
        }
        else if(Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    objectCount++;
                    prefabSpaceCounter += spaceBetweenPrefabs;
                    Instantiate(objectPrefab, new Vector3(0, prefabSpaceCounter, 0), Quaternion.identity, objectHolder.transform);
                    break;

                case TouchPhase.Moved:
                    objectCount++;
                    prefabSpaceCounter += spaceBetweenPrefabs;
                    Instantiate(objectPrefab, new Vector3(0, prefabSpaceCounter, 0), Quaternion.identity, objectHolder.transform);
                    break;

                case TouchPhase.Ended:
                    break;
            }
        }
    }

    public void SwitchSpawnable(GameObject newObject)
    {
        objectPrefab = newObject;
    }

}
