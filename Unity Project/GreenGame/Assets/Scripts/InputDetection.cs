using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.UI;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.XR.Interaction.Toolkit.UI;

public class InputDetection : MonoBehaviour
{
    public bool VRMode = false;

    public bool DesktopModeOverride = false; //Will force desktop mode (For when you have a headset connected but want to test the desktop mode)

    //XR Stuff
    public GameObject XRRig;
    public XRUIInputModule XRUIEventSystemComponent;
    public GameObject XRInteractionManagerInstance;
    public TrackedDeviceGraphicRaycaster canvasXRRaycaster;
    public GameObject VRCamera;

    //Desktop stuff
    public GameObject DesktopRig;
    public InputSystemUIInputModule UIEventSystemComponent;
    public GameObject DesktopCamera;

    //Generic stuff
    public GameObject UICamera;
    public GameObject playerCanvas;
    public GameObject stuffToMoveWithPlayer;

    private void Start()
    {
        StartCoroutine(LateStart(1f));
    }

    IEnumerator LateStart(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);

        var inputDevices = new List<InputDevice>();
        InputDevices.GetDevices(inputDevices);
        foreach (var device in inputDevices)
        {

            Debug.Log("Device found with name " + device.name + " and role '{1}'" + device.characteristics);
            if (device.isValid && XRSettings.isDeviceActive && !VRMode)
            {
                VRMode = DesktopModeOverride ? false : true; //If there is a connected device then as long as the override isn't true then it will set VRMode true
            }
        }

        if (VRMode)
        {
            Destroy(DesktopRig); //Band aid fix for desktop rig raycasting for seemingly no reason
        }

        //Enable/disable XR stuff
        XRRig.SetActive(VRMode ? true : false);
        XRInteractionManagerInstance.SetActive(VRMode ? true : false);
        XRUIEventSystemComponent.enabled = VRMode ? true : false;
        canvasXRRaycaster.enabled = VRMode ? true : false;

        //Parent generic stuff to the right places
        UICamera.transform.SetParent(VRMode ? VRCamera.transform : DesktopCamera.transform);
        UICamera.transform.position = Vector3.zero; //Zero out the cams position when moving it between parents
        playerCanvas.transform.SetParent(VRMode ? XRRig.transform : DesktopRig.transform);
        AudioListener.Destroy(VRMode ? VRCamera.gameObject.GetComponent<AudioListener>() : DesktopCamera.gameObject.GetComponent<AudioListener>());
        GameManager.instance.eyeRaycaster.gameObject.transform.SetParent(VRMode ? VRCamera.transform : DesktopCamera.transform);
        stuffToMoveWithPlayer.transform.SetParent(VRMode ? XRRig.transform : DesktopRig.transform);

        //Enable/disable Desktop stuff
        DesktopRig.SetActive(VRMode ? false : true);
        UIEventSystemComponent.enabled = VRMode ? false : true;

        //Set up XRSettings based on which mode we are in
        XRSettings.enabled = VRMode ? true : false;

        if (VRMode)
        {
            GameManager.instance.gameCamera = VRCamera;
        }
        else
        {
            GameManager.instance.gameCamera = DesktopCamera;
        }
    }

}
