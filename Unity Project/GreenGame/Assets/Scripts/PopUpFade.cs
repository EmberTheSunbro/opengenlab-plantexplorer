using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PopUpFade : MonoBehaviour
{
    [System.NonSerialized] public bool isFadingOut = false; //prevents fading out again if already fading out 

    Coroutine lastFadeCoroutine;

    private float fadeTime = 1f;

    public GameObject popUpUI;
    Image panel;
    TextMeshProUGUI popUpText;
   
    public void Start()
    {
        popUpText = popUpUI.GetComponentInChildren<TextMeshProUGUI>();
        panel = popUpUI.GetComponentInChildren<Image>();
    }

    public void FadeIn()
    {
        isFadingOut = false;

        if (lastFadeCoroutine != null) StopCoroutine(lastFadeCoroutine);
        lastFadeCoroutine = StartCoroutine(FadeCoroutine(0f, 1f, false));
    }

    public void FadeOut()
    {
        isFadingOut = true;

        if (lastFadeCoroutine != null) StopCoroutine(lastFadeCoroutine);
        lastFadeCoroutine = StartCoroutine(FadeCoroutine( 1f, 0f, true));
    }

    public IEnumerator FadeCoroutine(float startAlpha, float endAlpha, bool disableOnFinish)
    {
        float lerpTimer = 0f;
        float lerpTime = fadeTime;

        while (lerpTimer < lerpTime)
        {
            lerpTimer += Time.deltaTime;
            if (lerpTimer > lerpTime)
            {
                lerpTimer = lerpTime;
            }

            // Ease in
            float t = lerpTimer / lerpTime;
            t = Mathf.Sin(t * Mathf.PI * 0.5f); 
            
            float alpha = Mathf.Lerp(startAlpha, endAlpha, t);
            panel.color = new Color(1f, 1f, 1f, alpha);
            popUpText.color = new Color(1f, 1f, 1f, alpha);
            yield return null;
        }

        isFadingOut = false;
        if (disableOnFinish)
        {
            popUpUI.SetActive(false);
            GetComponent<Cell_Info>().SetFalseShowInfo();
        }
    }
}
