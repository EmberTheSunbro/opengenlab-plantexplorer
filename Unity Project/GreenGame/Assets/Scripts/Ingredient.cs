using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ingredient : MonoBehaviour
{
    public PuzzleSystemExtraction.IngredientType ingredientType;

    public PuzzleSlot puzzleSlot;

    public GameObject buttonToDisable;

    [SerializeField]
    private SlotManager slotManager;

    public void SetSlot(PuzzleSlot _puzzleSlot)
    {
        // Do not call this directly 

        puzzleSlot = _puzzleSlot;
    }

    public void DisableButton()
    {
        slotManager.HideIngredientButtons();
        //buttonToDisable.gameObject.SetActive(false);
    }

    public void EnableButton()
    {
        slotManager.ShowIngredientButtons();
        if (slotManager.checkpoint1 == true && slotManager.checkpoint2 == false && slotManager.checkpoint3 == false)
        {
            slotManager.HideIngredientButtons();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        PuzzleSlot _slot = other.gameObject.GetComponent<PuzzleSlot>(); ;

        if (_slot != null && _slot.storedIngredient == null)
        {
            InventoryManager.instance.DropIngredient();
            this.gameObject.transform.position = other.gameObject.transform.position;
            this.gameObject.transform.rotation = other.gameObject.transform.rotation;
            _slot.SetStoredIngredient(this);
            //slotManager.HandleSlotsFull();
            InventoryManager.instance.hologramSlots.SetActive(false);
            EnableButton();
            //Debug.Log("Collided");
        }
    }
}

