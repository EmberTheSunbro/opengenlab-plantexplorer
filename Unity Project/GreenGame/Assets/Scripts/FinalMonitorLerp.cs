using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalMonitorLerp : MonoBehaviour
{
    [SerializeField]
    CoroutineManager coroutineManager;

    public Transform monitorLerpStartPos;

    [SerializeField]
    Transform monitorLerpMidPos;

    [SerializeField]
    Transform monitorFinalMidPos;

    public Vector3 originalMonitorPos;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Coroutine to move to final position
    public void MoveMonitorToSuccessPosition()
    {
        StartCoroutine(MonitorLerpCoroutine(monitorLerpStartPos, monitorLerpMidPos.transform.position));
        coroutineManager.BeginWaitCoroutine(() =>
        {
            StartCoroutine(MonitorLerpCoroutine(monitorLerpMidPos, monitorFinalMidPos.transform.position));
        }, 5f);
    }

    // Coroutine to move back to original position
    public void MoveMonitorBackToOriginalPosition()
    {
        StartCoroutine(MonitorLerpCoroutine(monitorFinalMidPos, monitorLerpMidPos.transform.position));
        coroutineManager.BeginWaitCoroutine(() =>
        {
            StartCoroutine(MonitorLerpCoroutine(monitorLerpMidPos,originalMonitorPos));
            
        }, 5f);
        coroutineManager.BeginWaitCoroutine(() =>
        {
            monitorLerpStartPos.rotation = Quaternion.Euler(0, -32.131f, 0);
        }, 10f);

    }

    //Lerp coroutine for moving montiors back and forth
    public IEnumerator MonitorLerpCoroutine(Transform monitorStartPos, Vector3 monitorEndPos)
    {
        Vector3 startPos = monitorStartPos.transform.position;
        Vector3 endPos = monitorEndPos;

        float lerpTimer = 0f;
        float lerpTime = 5f;


        while (lerpTimer < lerpTime)
        {
            lerpTimer += Time.deltaTime;
            if (lerpTimer > lerpTime)
            {
                lerpTimer = lerpTime;
            }

            // Lerp math
            float t = lerpTimer / lerpTime;
            t = Mathf.Sin(t * Mathf.PI * 0.5f);

            monitorLerpStartPos.transform.position = Vector3.Lerp(startPos, endPos, t);
            //Debug.Log(ingredientToMove.transform.position);
            yield return null;
        }
    }
}
