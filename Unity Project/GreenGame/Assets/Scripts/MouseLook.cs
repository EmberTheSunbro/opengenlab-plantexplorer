using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class MouseLook : MonoBehaviour
{

    private InputMaster controls;

    public float mouseSensitivity = 100f;

    private Vector2 mouseLook;

    private float xRotation;
    private float yRotation;

    private void Awake()
    {
        controls = new InputMaster();
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void FixedUpdate()
    {
        Look();
    }

    private void Look()
    {
        mouseLook = controls.DesktopPlayer.Look.ReadValue<Vector2>(); //Reads the mouse delta to mouseLook

        float mouseX = mouseLook.x * mouseSensitivity * Time.deltaTime;
        float mouseY = mouseLook.y * mouseSensitivity * Time.deltaTime;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90); //Clamps value so can't look all the way around (up and down)
        yRotation += mouseX;

        transform.localRotation = Quaternion.Euler(xRotation, yRotation, 0);
    }

    private void OnEnable() //Prevents memory leaks in the action system
    {
        controls.Enable();
    }

    private void OnDisable()
    {
        controls.Disable();
    }
}
