using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSelection : MonoBehaviour
{
    // Reference to animator of microscope
    public GameObject Microscope;
    Animator microscope_anim;

    // Reference to animator of DNA sequencer
    public GameObject Sequencer;
    Animator sequencer_anim;

    SceneLoader sceneLoader;

    private void Awake()
    {
        microscope_anim = Microscope.GetComponent<Animator>();
        sequencer_anim = Sequencer.GetComponent<Animator>();
    }

    // Start is called before the first frame update
    void Start()
    {
        sceneLoader = GameManager.instance.sceneLoader;
    }

    // Update is called once per frame
    void Update()
    {
        MoveMicroscope();
        MoveSequencer();
    }

    public void MoveMicroscope()
    {
        if(sceneLoader.isLoadingMicroscopeScene == true)
        {
            microscope_anim.SetBool("isMicroscopeMoving", true);
        }
        else
        {
            microscope_anim.SetBool("isMicroscopeMoving", false);
        }
        
    }

    public void MoveSequencer()
    {
        if (sceneLoader.isLoadingSequencerScene == true)
        {
            sequencer_anim.SetBool("isMovingSequencer", true);
        }
        else
        {
            sequencer_anim.SetBool("isMovingSequencer", false);
        }
    }

    
}

