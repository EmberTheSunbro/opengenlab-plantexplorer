using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{
    public static InventoryManager instance;
    public GameObject hologramSlots;

    public Ingredient ingredientPickedUp;

    [SerializeField]
    Transform cam; // Main Camera

    [SerializeField]
    private SlotManager slotManager;

    private void Start()
    {
        InventoryManager.instance = this;

        if(LocalReferenceManager.Instance == null)
        {
            return;
        }

        hologramSlots = LocalReferenceManager.Instance.hologramSlots;
        slotManager = LocalReferenceManager.Instance.slotManager;

    }

    void OnEnable()
    {
       // ResetReferences();
    }

    //private void Awake()
    //{
    //    if(instance != null)
    //    {
    //        Destroy(gameObject);
    //    }
    //    else
    //    {
    //        instance = this;
    //        DontDestroyOnLoad(gameObject);
    //    }
    //}

    private void Update()
    {
        if (cam != GameManager.instance.gameCamera)
        {
            cam = GameManager.instance.gameCamera.transform;
        }

        if (ingredientPickedUp != null)
        {
            hologramSlots.SetActive(true);
            ingredientPickedUp.transform.position = cam.transform.position + cam.transform.forward * 3f;
        }
        //Debug.Log(ingredientPickedUp);
        
    }

    public void PickUpIngredient(Ingredient ingredient)
    {
        PuzzleSlot previousSlot = ingredient.puzzleSlot;

        if(previousSlot != null)
        {
            previousSlot.SetStoredIngredient(null);
        }
        ingredientPickedUp = ingredient;

    }

    public void DropIngredient()
    {
        ingredientPickedUp = null;
    }

    public void ResetReferences()
    {
        if (LocalReferenceManager.Instance == null)
        {
            return;
        }

        hologramSlots = LocalReferenceManager.Instance.hologramSlots;
        slotManager = LocalReferenceManager.Instance.slotManager;
    }

}
