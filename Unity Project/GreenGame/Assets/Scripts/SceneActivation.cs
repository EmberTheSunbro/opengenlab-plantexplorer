using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;
using UnityEngine.SceneManagement;

public class SceneActivation : MonoBehaviour
{
    SceneLoader sceneLoader;

    MainSceneTransfer mainSelectionScene;

    public bool mainScene;
    public bool microscopeScene;
    public bool cyclerScene;
    public bool sequencerScene;

    public GameObject childObjectToActivate;

    public List<GameObject> additionalObjectsToActivate;

    public bool lateRun = false;
    bool hasLateRun = false;

    public string currentScene;

    private void Start()
    {
        validateObjectActivation(currentScene);
    }

    private void OnEnable()
    {
        SceneLoader.OnSceneTransfer += NewScene;
    }

    private void OnDisable()
    {
        SceneLoader.OnSceneTransfer -= NewScene;
    }

    private void NewScene(string sceneName)
    {
        currentScene = sceneName;
        validateObjectActivation(currentScene);
        hasLateRun = false;
    }


    public void validateObjectActivation(string sceneName)
    {
        if (sceneName == "Microscope") //For the scene being changed to
            childObjectToActivate.SetActive(microscopeScene);   //If it's supposed to be active in the scene or not

        if (sceneName == "Sequencer")
            childObjectToActivate.SetActive(sequencerScene);

        if (sceneName == "ThermalCycler")
            childObjectToActivate.SetActive(cyclerScene);

        if (sceneName == "MainScene")
            childObjectToActivate.SetActive(mainScene);

        //if(additionalObjectsToActivate != null)
        //{
        //    foreach (GameObject additionalObject in additionalObjectsToActivate)
        //    {
        //        if (sceneName == "Microscope") //For the scene being changed to
        //            additionalObject.SetActive(microscopeScene);   //If it's supposed to be active in the scene or not

        //        if (sceneName == "Sequencer")
        //            additionalObject.SetActive(sequencerScene);

        //        if (sceneName == "ThermalCycler")
        //            additionalObject.SetActive(cyclerScene);

        //        if (sceneName == "MainScene")
        //            additionalObject.SetActive(mainScene);
        //    }
        //}
        
    }

}
