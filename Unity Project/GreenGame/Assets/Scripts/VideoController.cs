using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoController : MonoBehaviour
{
    private VideoPlayer videoPlayer;

    public Material oldMaterialRef;
    public Material newMaterialRef;

    [SerializeField]
    private GameObject screenCanvas;

    
    public bool isVideoPlaying = false;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Renderer>().material = oldMaterialRef;
        videoPlayer = GetComponent<VideoPlayer>();
    }

    // Update is called once per frame
    void Update()
    {
        // Plays video if condition is true
        //if (isVideoPlaying)
        //{
        //    screenCanvas.SetActive(false);
        //    GetComponent<Renderer>().material = newMaterialRef;
        //    videoPlayer.Play();

        //}
        //else
        //{
        //    screenCanvas.SetActive(true);
        //    GetComponent<Renderer>().material = oldMaterialRef;
        //    videoPlayer.Stop();
        //}
    }

    public void CheckVideoPLaying()
    {
        if (isVideoPlaying)
        {
            screenCanvas.SetActive(false);
            GetComponent<Renderer>().material = newMaterialRef;
            videoPlayer.Play();
            Debug.Log("Playing");
        }
        else
        {
            screenCanvas.SetActive(true);
            GetComponent<Renderer>().material = oldMaterialRef;
            videoPlayer.Stop();
        }
    }
}
