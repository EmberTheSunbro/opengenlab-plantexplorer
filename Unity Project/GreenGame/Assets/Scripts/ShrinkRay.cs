using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShrinkRay : MonoBehaviour
{

    bool teleportMode;

    public List<float> distancePositions; //Here we set up the different zoom amounts for this given shrinkray
    public List<float> scaleAmount;

    public GameObject worldRoot; //World root being scaled from this pivot
    public GameObject targetCellPivot; //The cell being targeted to scale from

    public bool zoomingIn;
    public bool zoomingOut;

    [SerializeField]
    float currentScale = 1;
    public float maxScale = 5; //How many times larger will it get
    public float minScale = 1;

    public float scaleSpeed;

    private void Start()
    {
        currentScale = worldRoot.transform.localScale.x;
        maxScale = currentScale * maxScale;
        minScale = currentScale * minScale;
    }

    private void Update()
    {
        if (zoomingIn)
        {
            if (currentScale < maxScale)
            {
                currentScale += Time.deltaTime * scaleSpeed;
            }
        }
        else if (zoomingOut)
        {
            if (currentScale > minScale)
            {
                currentScale -= Time.deltaTime * scaleSpeed;
            }
        }

        ScaleAround(worldRoot, targetCellPivot.transform.position, new Vector3(currentScale, currentScale, currentScale));
    }

    public void ZoomIn()
    {
        zoomingIn = true;
    }

    public void StopZoomingIn()
    {
        zoomingIn = false;
    }

    public void ZoomOut()
    {
        zoomingOut = true;
    }

    public void StopZoomingOut()
    {
        zoomingOut = false;
    }

    public void ScaleAround(GameObject toBeScaled, Vector3 pivot, Vector3 newScale)
    {
        Vector3 A = toBeScaled.transform.localPosition;
        Vector3 B = pivot;

        Vector3 C = A - B; //Difference between object pivot to desired pivot/origin

        float RS = newScale.x / toBeScaled.transform.localScale.x; // relative scale factor

        Vector3 FP = B + C * RS; //Calculate final position post-scale

        toBeScaled.transform.localScale = newScale;
        toBeScaled.transform.localPosition = FP;
    }
}
