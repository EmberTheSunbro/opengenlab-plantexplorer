using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class ZoomManager : MonoBehaviour
{
    public bool teleportMode = true; //Is it teleporting or zooming
    public float teleportFadeTime = 1; //Screen fade time on teleport
    public Transform worldRoot; //The object to scale everything within
    Transform playerObject;

    public ToggleGroup ZoomToggleUI; //The toggle UI to refrence
    public List<Toggle> toggles; //Stores a corresponding list of toggles
    public Toggle currentToggle; //Currently zoomed to this toggle

    Dictionary<Toggle, ZoomLevel> ToggleRelations = new Dictionary<Toggle, ZoomLevel>(4); //For use with toggles as keys for zoom levels
    
    [SerializeField]
    public List<ZoomLevel> zoomLevels; //Stores a list of zoom level structs
    public ZoomLevel currentZoomLevel;
    public ZoomLevel targetZoomLevel;
    public Transform zoomPlatform;

    public GameObject ZoneBarrierColliderParent;
    bool switching = false; //True if in zoom mode and currently switching to get closer to the zoom mode we want to be in
    bool zooming = false; //True if zooming

    int currentIndex;
    int targetIndex;
    int nextTargetIndex;

    Image blinkImage;

    public Camera zoomPortalCamera;
    public ParticleSystem portalParticles;

    bool portalMode = true;
    bool wasOff = true;

    [System.Serializable] public struct ZoomLevel
    {
        [Tooltip("The layer name used on the info popups colliders.")]
        public string levelName;

        [Tooltip("Used for recording the initial scale of the object")]
        public Vector3 rootScale;

        //Stuff to be activated after the barrier pass if in zoom mode
        [Tooltip("Parent of all the popup UIs for a given level")]
        public GameObject levelPopupUIParent;

        [Tooltip("The zoom level to scale")]
        public GameObject localRoot;
        [Tooltip("The object to scale around")]
        public Transform zoomRefrencePoint;

        [Tooltip("The layer on which this level is visible")]
        public int Layer;
        public PortalManager thisPortal; 

        [Tooltip("Skybox to be used in layer.")]
        public Material layerSkybox;

        [Tooltip("Amount of time for the out transition to this element")]
        public float ZoomIntoTimer;
        public Vector3 ZoomIntoScale;
    }

    private void Start()
    {
        toggles = new List<Toggle>(GameManager.instance.zoomToggleGroup.GetComponentsInChildren<Toggle>()); //Sets up the toggles using the zoomToggles on the playerCanvas
        ZoomToggleUI = GameManager.instance.zoomToggleGroup.GetComponent<ToggleGroup>();

        for (int i = 0; i < zoomLevels.Count; i++)
        {
            ToggleRelations.Add(toggles[i], zoomLevels[i]); //Sets up toggles as keys for the zoom levels for easy refrence
        }

        if (playerObject == null) //Updates the playerobject refrence
        {
            if (GameManager.instance.playerRig == null)
            {
                InputDetection ID = GameManager.instance.inputDetection;
                if (ID.VRMode)
                {
                    playerObject = ID.XRRig.transform;
                }
                else
                {
                    playerObject = ID.DesktopRig.transform;
                }
            }
            else
            {
                playerObject = GameManager.instance.playerRig.transform;
            }
        }

        for (int i = 0; i < zoomLevels.Count; i++)
        {
            ZoomLevel temp = zoomLevels[i];
            //temp.localRoot.SetActive(false);
            if(i == 0 || i == 1)
            temp.localRoot.SetActive(false);
            temp.levelPopupUIParent.SetActive(false);
            temp.rootScale = temp.localRoot.gameObject.transform.localScale;
        }

    }

    private void FixedUpdate() //Fixed update to have constant movement and no framerate based interference
    {
        wasOff = false;

        if(teleportMode != GameManager.instance.teleportToggle) //If teleport mode is off and zoom toggle is off
        {
            teleportMode = GameManager.instance.teleportToggle;
        }

        if (!GameManager.instance.mainMenuUI.activeSelf) //If it isn't active set it active for the next block of code and then back
        {
            GameManager.instance.mainMenuUI.SetActive(true);

            wasOff = true;
        }

        ///Check to see if it needs to adjust the targetZoomLevel
        Toggle nextToggle = ZoomToggleUI.GetFirstActiveToggle();

            if (!currentToggle && nextToggle) //If it hasn't set up a toggle yet 
            {
                currentToggle = nextToggle; //then use the current one and activate the corresponding gameobjects
                currentZoomLevel = ToggleRelations[nextToggle]; //use the toggle refrence dictionary to set the current level to this toggle

                currentZoomLevel.localRoot.SetActive(true); //Activate the two levels necessary
                
                if (portalMode)
                {
                    zoomLevels[zoomLevels.IndexOf(currentZoomLevel) + 1].localRoot.SetActive(true);
                }       

                GameManager.instance.gameCamera.GetComponent<Camera>().cullingMask = 1 << currentZoomLevel.Layer;
                zoomPortalCamera.cullingMask = 1 << (currentZoomLevel.Layer + 1);
                currentZoomLevel.levelPopupUIParent.SetActive(true);
            }

            if (nextToggle != currentToggle && nextToggle != null) //If the toggle has been changed since it was last updated
            {
                currentZoomLevel.levelPopupUIParent.SetActive(false); //Turn off the layer we are ons info UI parent
                targetZoomLevel = ToggleRelations[nextToggle]; //Sets the new target zoom level struct to be used
                switching = true;
                GameManager.instance.popupOpen = null;
                currentToggle = nextToggle;
            }

            if (wasOff)
            {
                GameManager.instance.mainMenuUI.SetActive(false);
            }

            if (switching) //If switching was turned on but we haven't reached our target zoom level (At which point it would be turned off again)
            {
                if (teleportMode) //If it's in the middle of changing position and in teleport mode
                {
                    if (GameManager.instance.screenBlackedOut)
                    {
                        Teleport(currentZoomLevel, targetZoomLevel); //Execute the teleport
                        currentZoomLevel.levelPopupUIParent.SetActive(true);
                        switching = false;
                    }
                    else if (!GameManager.instance.blackScreenToggle)
                    {
                        GameManager.instance.blackScreenToggle = true;
                    }
                }
                //If it isn't in teleport mode AND hasn't fully zoomed in yet AND isn't currently in a zoom transition
                else if (currentZoomLevel.levelName != targetZoomLevel.levelName && !zooming)
                {
                    currentIndex = zoomLevels.IndexOf(currentZoomLevel); //Find the positions in the list
                    targetIndex = zoomLevels.IndexOf(targetZoomLevel);

                    GameManager.instance.mainMenuUI.SetActive(false);

                    if (currentIndex < targetIndex) //If we have not reached target index yet initiate the zoom for this current level
                    {
                        nextTargetIndex = currentIndex + 1; //Set the target index to an increment of one for this zoom
                        zooming = true;
                        StartCoroutine(ZoomingLerp(currentZoomLevel)); //Zoom
                    }
                    else if (currentIndex > targetIndex) //If we are past target index then teleport backwards to the index we want to be at
                    {
                        if (GameManager.instance.screenBlackedOut)
                        {
                        Teleport(currentZoomLevel, targetZoomLevel); //Execute the teleport
                        currentZoomLevel.levelPopupUIParent.SetActive(true);
                        switching = false;
                        }
                        else if (!GameManager.instance.blackScreenToggle)
                        {
                        GameManager.instance.blackScreenToggle = true;
                        }
                    }
                }
                else if (currentZoomLevel.levelName == targetZoomLevel.levelName && !zooming)
                {
                    currentZoomLevel.levelPopupUIParent.SetActive(true);
                    switching = false;
                }
            }
    }

    IEnumerator ZoomingLerp(ZoomLevel levelToZoom)
    {
        float timeElapsed = 0;

        GameObject objectToScale = levelToZoom.localRoot;
        float transitionDuration = levelToZoom.ZoomIntoTimer;
        Vector3 startScale = levelToZoom.rootScale;
        Vector3 endScale = levelToZoom.ZoomIntoScale;
        Transform zoomRefrencePoint = levelToZoom.zoomRefrencePoint;

        AudioManager.instance.Play("Thrusters");

        while (timeElapsed < transitionDuration)
        {
            //valueToLerp = Mathf.Lerp(startValue, endValue, timeElapsed / lerpDuration)
            ScaleAround(objectToScale.transform, zoomRefrencePoint.position, Vector3.Lerp(startScale, endScale, EaseIn(timeElapsed / transitionDuration)));

            timeElapsed += Time.deltaTime;

            yield return null;
        }

        //valueToLerp = endValue so that there is no minute calculation issues left over from the timer not being 100% accurate
        ScaleAround(objectToScale.transform, zoomRefrencePoint.position, endScale);

        if (!portalMode)
        {
            while (!GameManager.instance.screenBlackedOut)
            {
                if (!GameManager.instance.blackScreenToggle)
                {
                    GameManager.instance.blackScreenToggle = true;
                }

                yield return null;
            }
        }

        ScaleAround(objectToScale.transform, zoomRefrencePoint.position, startScale);

        AudioManager.instance.StopPlaying("Thrusters");
        AudioManager.instance.Play("ThrusterEnd");

        if (zoomLevels[nextTargetIndex].levelName == "Macro")
        {
            AudioManager.instance.Play("Wind");
        }
        else
        {
            AudioManager.instance.StopPlaying("Wind");
        }

        AudioManager.instance.Play("BarrierCross");

        Teleport(currentZoomLevel, zoomLevels[nextTargetIndex]); //Execute the teleport
        zooming = false;

    }

    public void Teleport(ZoomLevel zoomLevelAt, ZoomLevel zoomLevelGoingTo)
    {
        zoomLevelAt.localRoot.SetActive(false);
        zoomLevelGoingTo.localRoot.SetActive(true);

        //GameManager.instance.mainMenuUI.SetActive(false);

        if (portalMode)
        {
            if (zoomLevelGoingTo.levelName != "DNA")
            {
                zoomLevels[zoomLevels.IndexOf(zoomLevelGoingTo) + 1].localRoot.SetActive(true);
                zoomPortalCamera.cullingMask = 1 << (zoomLevelGoingTo.Layer + 1);
                zoomPortalCamera.GetComponent<Skybox>().material = zoomLevels[zoomLevels.IndexOf(zoomLevelGoingTo) + 1].layerSkybox;
            }

            if (zoomLevelAt.levelName != "DNA")
            {
                zoomLevelAt.thisPortal.ZeroOutScale(); //Done with the portal make it small again
            }
        }

        GameManager.instance.gameCamera.GetComponent<Camera>().cullingMask = 1 << zoomLevelGoingTo.Layer; //Sets the culling mask for the current level we are at
        ScaleAround(zoomLevelGoingTo.localRoot.transform, zoomLevelGoingTo.zoomRefrencePoint.position, zoomLevelGoingTo.rootScale);

        if (zoomLevelGoingTo.layerSkybox) //If there is a skybox for the layer we are headed to then do the switch
        {
            RenderSettings.skybox = zoomLevelGoingTo.layerSkybox;
        }

        currentZoomLevel = zoomLevelGoingTo;

        GameManager.instance.mainMenuUI.SetActive(false);

        GameManager.instance.blackScreenToggle = false; //Fade back in on completing teleport

        
    }

    public void ScaleAround(Transform toBeScaled, Vector3 pivot, Vector3 newScale)
    {
        Vector3 A = toBeScaled.localPosition;
        Vector3 B = pivot;

        Vector3 C = A - B; //Difference between object pivot to desired pivot/origin

        float RS = newScale.x / toBeScaled.localScale.x; // relative scale factor

        Vector3 FP = B + C * RS; //Calculate final position post-scale

        toBeScaled.localScale = newScale;
        toBeScaled.localPosition = FP;
    }

    public static float EaseIn(float t)
    {
        return t * t * t * t;
    }

}
