using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SpecialCheckpointAlternateButton : SpecialSelectionButton
{
    //Assign text to appear during the onpointer enter
    public Animator hoverPanelTextToActivate;

    bool currentlySelected = false; //if set true then this needs to become not selectable and also stay at alpha 1

    CheckpointManager cm;

    public int thisCheckpoint;

    protected override void Awake()
    {
        base.Awake();
    }
    private void Start()
    {
        cm = GameManager.instance.checkpointManager;
    }

    public override void PointerEnter()
    {
        hoverPanelTextToActivate.SetTrigger("Appear");
        if (!currentlySelected)
        {
            base.PointerEnter();
        }
    }

    public override void PointerExit()
    {
        hoverPanelTextToActivate.SetTrigger("Disappear");
        if (!currentlySelected)
        {
            base.PointerExit();
        }
    }

    public override void PointerClick()
    {
        base.PointerClick();
        this.gameObject.GetComponent<Button>().onClick.Invoke();
    }
}
