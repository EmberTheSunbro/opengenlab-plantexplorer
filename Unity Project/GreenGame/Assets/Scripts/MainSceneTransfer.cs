using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainSceneTransfer : MonoBehaviour
{
    public Animator transition;

    [SerializeField]
    private GameObject[] hideableObjects;

    SceneLoader sceneLoader;


    private void OnEnable()
    { 
        SceneLoader.OnEarlySceneTransfer += MainScene;
    }

    private void OnDisable()
    {
        SceneLoader.OnEarlySceneTransfer -= MainScene;
    }

    public void SetHideableObjectState(int[] indexesToSkip)
    {
        // Hiding object based on indexes
        for (int i = 0; i < hideableObjects.Length; i++)
        {
            if (IsSkippedIndex(i)) continue;
            hideableObjects[i].SetActive(false);
        }

        // Finds whether input index is inside the indexes to skip 
        bool IsSkippedIndex(int currentIndex)
        {
            for (int j = 0; j < indexesToSkip.Length; j++)
            {
                if (currentIndex == indexesToSkip[j])
                {
                    return true;
                }
            }
            return false;
        }
    }

    private void MainScene(string sceneName)
    {
        if (sceneName == "Microscope")
        {
            //Call hideable object and animation
            SetHideableObjectState(new int[] { 0 });
        }
        if (sceneName == "Sequencer")
        {
            //Call hideable object and animation
            SetHideableObjectState(new int[] { 4 });
        }
        if (sceneName == "ThermalCycler")
        {
            //Call hideable object and animation
            SetHideableObjectState(new int[] { 2 });
        }
    }
}
