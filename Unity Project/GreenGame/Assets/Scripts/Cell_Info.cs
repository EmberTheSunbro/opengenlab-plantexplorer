using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Cell_Info : MonoBehaviour
{

    GameObject thisCellPopup;

    private Coroutine lastCoroutine;

    private bool isShowingInfo = false;

    public PopUpFade popUpFadeScript;


    public void ShowCell(bool enabled)
    {
        // Only called when players are looking at the cell part
        thisCellPopup.SetActive(enabled);

        //if (showPanelButton == null) return;
        //showPanelButton.ShowSettingPanel();

        if (lastCoroutine != null)
        {
            StopCoroutine(lastCoroutine);
        }

        if (!isShowingInfo)
        {
            Debug.Log("Fading");
            if (enabled) GetComponent<PopUpFade>().FadeIn();
            isShowingInfo = true;
        }

    }

    public void HideCell()
    { 

        if (lastCoroutine != null)
        {
            StopCoroutine(lastCoroutine);
        }

        lastCoroutine = StartCoroutine(WaitCoroutine());
    }

    private IEnumerator WaitCoroutine()
    {
        yield return new WaitForSeconds(0.5f);
        GetComponent<PopUpFade>().FadeOut();
    }

    public void SetFalseShowInfo()
    {
        isShowingInfo = false;
    }
}
