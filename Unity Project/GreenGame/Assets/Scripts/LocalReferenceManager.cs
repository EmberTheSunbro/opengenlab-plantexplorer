using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LocalReferenceManager : MonoBehaviour
{
    public static LocalReferenceManager Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
    }

    public Camera mainCam;

    public GameObject hologramSlots;

    public SlotManager slotManager;

    private void Start()
    {

    }
}
