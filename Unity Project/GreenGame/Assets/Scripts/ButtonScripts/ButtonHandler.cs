using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonHandler : MonoBehaviour
{
    private float cancelHighlightAngle = 15f;

    private float verticalActivationAngle;

    //public GameObject ceiling;

    EyeRaycaster eyeRaycaster;

    List<SpecialSelectionButton> highlightedButtons = new List<SpecialSelectionButton>();
    
    List<SpecialSelectionButton> oldHighlightedButtons = new List<SpecialSelectionButton>();

    Camera mainCamera;

    float lookTimer;
    float lookTime = 2.5f;

    float buttonCooldownTimer;

    SpecialSelectionButton oldButton;
    SpecialSelectionButton currentButton;

    private void Start()
    {
        eyeRaycaster = GameManager.instance.eyeRaycaster;
        lookTimer = lookTime;
        //Debug.Log(eyeRaycaster);
    }

    private void Update()
    {
        if(mainCamera == null)
        {
            try
            {
                mainCamera = GameManager.instance.gameCamera.GetComponent<Camera>();
            }
            catch
            {
                Debug.Log("MainCamera isn't assigned yet");
            }
        }

        currentButton = null; //We dont have a button yet this round

        SpecialSelectionButton button = null;

        for (int i = 0; i < eyeRaycaster.raycastResults.Count; i++)
        {

            if(eyeRaycaster.raycastResults[i].gameObject != null)
            {
                 button = eyeRaycaster.raycastResults[i].gameObject.GetComponent<SpecialSelectionButton>();
            }

            if(button != null) //If we find a selection button
            {
                currentButton = button;
                break;
            }
        }

        if (oldButton != currentButton && currentButton != null)
        {
            currentButton.PointerEnter(); //Enter the new button
            lookTimer = lookTime; //Reset the timer because new button
            buttonCooldownTimer = 0;
            
            if(oldButton != null)
            {
                oldButton.PointerExit();
            }
            oldButton = currentButton;
        }
        else if (currentButton == null && oldButton != null) //If we don't have a current button still then exit the old button and void it
        {
            oldButton.PointerExit();
            lookTimer = lookTime; //Reset the timer because no butto
            oldButton = null;
        }
        else if(oldButton == currentButton && buttonCooldownTimer <= 0 && oldButton != null) //if its the same button as last frame
        {
            lookTimer -= Time.deltaTime;
            if(lookTimer <= 0) //If the timer has counted down
            {
                lookTimer = lookTime; //Reset the timer
                currentButton.PointerClick(); //Click that button
                buttonCooldownTimer = lookTime; //Set the cooldown to click button again
            }
        }
        else if(buttonCooldownTimer > 0)
        {
            buttonCooldownTimer -= Time.deltaTime;
        }

        // Checking if raycast hits a button
        //for (int i = 0; i < eyeRaycaster.raycastResults.Count; i++)
        //{
        //    SpecialSelectionButton button = eyeRaycaster.raycastResults[i].gameObject.GetComponent<SpecialSelectionButton>();

        //    if (button != null)
        //    {
        //        button.PointerEnter();
        //        if (!highlightedButtons.Contains(button)) highlightedButtons.Add(button);
        //    }
        //}

        

        //for (int i = 0; i < oldHighlightedButtons.Count; i++) // For each oldhighlighted button
        //{
        //    bool verified = false;

        //    for (int a = 0; a < highlightedButtons.Count; a++) //Compare it to each of the new buttons
        //    {
        //        if(oldHighlightedButtons[i] == highlightedButtons[a]) //If it is one
        //        {
        //            verified = true;
        //            break;
        //        }
        //    }

        //    if (!verified) //If it never found a pair then exit
        //    {
        //        oldHighlightedButtons[i].PointerExit();
        //    }
        //}

        //oldHighlightedButtons = highlightedButtons;

        //if (highlightedButtons.Count > 0)
        //{
        //    lookTimer -= Time.deltaTime;
        //    if (lookTimer <= 0)
        //    {
        //        lookTimer = lookTime;
        //        highlightedButtons[0].PointerClick();
        //        highlightedButtons.Clear();
        //    }
        //}

        //for (int j = 0; j < highlightedButtons.Count; j++)
        //{
        //    Vector3 cameraDirection = mainCamera.transform.forward;

        //    //Changes this because using a canvas position is not good practice
        //    Vector3 cameraToButtonDirection = highlightedButtons[j].transform.position - mainCamera.transform.position;
        //    //Vector3 cameraToButtonDirection = highlightedButtons[j].worldspaceCanvas.transform.position - mainCamera.transform.position;

        //    float angleBetweenCameraAndButton = Vector3.Angle(cameraDirection, cameraToButtonDirection);
        //    //Debug.Log(angleBetweenCameraAndButton);

        //    if (angleBetweenCameraAndButton > cancelHighlightAngle)
        //    {
        //        //Unhighlight the button
        //        highlightedButtons[j].PointerExit();
        //        highlightedButtons.RemoveAt(j);

        //        lookTimer = lookTime;
        //    }
        //}

        //To find vertical height angle
        //verticalActivationAngle = degreesBetweenVectors(Camera.main.transform.position, ceiling.transform.position);

        // if (verticalActivationAngle < 30)
        // {
        //     //Call Scene loader functions to show menus here


        // }
        //Debug.Log(verticalActivationAngle);

        /*RaycastHit hit;
        Ray ray = mainCamera.ScreenPointToRay(new Vector2(Screen.width / 2f, Screen.height / 2f)); 

        if (Physics.Raycast(ray, out hit))
        {
            Debug.Log("raycast targ " + hit.transform.gameObject.name);
            SpecialSelectionButton button = hit.transform.GetComponent<SpecialSelectionButton>();

            if (button != null)
            {
                Debug.Log("button found");
                //button.OnPointerClick(new PointerEventData(EventSystem.current));
                button.PointerClick(); 
            }
            // Do something with the object that was hit by the raycast.
        }*/
    }

    // For finding the verticle angle between two game objects
    float degreesBetweenVectors(Vector3 vA, Vector3 vB)
    {
        vA.Normalize();
        vB.Normalize();
        float ADotB = Vector3.Dot(vA, vB);
        float radians = Mathf.Acos(ADotB);
        return radians * Mathf.Rad2Deg;
    }


}
