using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ThermalCyclerButton : SpecialSelectionButton
{
    /*public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);
        SceneLoader.instance.LoadThermalCyclerScene();
    }*/

    public override void PointerClick()
    {
        base.PointerClick();
        GameManager.instance.sceneLoader.LoadThermalCyclerScene();
    }
}
