using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MortarPestleButton : SpecialSelectionButton
{
    [SerializeField]
    PuzzleSystemExtraction puzzleSystemExtraction;

    [SerializeField]
    GameObject nextExtractionButton;

    [SerializeField]
    GameObject[] nextExtractionSetButtons;

    [SerializeField]
    Animator animator;

    [SerializeField]
    Animator pipetteMotorAnimator;

    [SerializeField]
    Animator pcrTubeMortarAnimator;

    [SerializeField]
    CoroutineManager coroutineManager;

    public override void PointerClick()
    {
        //Debug.Log("nnn");
        base.PointerClick();
        AudioManager.instance.Play("AnimationStart");
        AudioManager.instance.Play("Mortar");
        this.gameObject.SetActive(false);
        animator.SetBool("Active",true);
        pipetteMotorAnimator.SetBool("Active", true);
        pcrTubeMortarAnimator.SetBool("Active", true);
        coroutineManager.BeginWaitCoroutine(() =>
        {
            nextExtractionButton.SetActive(true);
            ShowRestBufferButtons();
            animator.SetBool("Active", false);
            pipetteMotorAnimator.SetBool("Active", false);
            pcrTubeMortarAnimator.SetBool("Active", false);
        }, 14f);
        puzzleSystemExtraction.PutInIngredient(PuzzleSystemExtraction.IngredientType.MortarPestle);
    }

    void ShowRestBufferButtons()
    {
        for (int i = 0; i < nextExtractionSetButtons.Length; i++)
        {
            nextExtractionSetButtons[i].SetActive(true);
        }
    }
}
