using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveSupernatantBtn : SpecialSelectionButton
{
    [SerializeField]
    PuzzleSystemExtraction puzzleSystemExtraction;

    [SerializeField]
    GameObject[] currentTools;

    [SerializeField]
    GameObject[] nextSetTools;

    public GameObject nextToolBtn;

    [SerializeField]
    CoroutineManager coroutineManager;

    [SerializeField]
    Animator pipetteSupernatantAnimator;

    [SerializeField]
    Animator pcrTubeSupernatantAnimator;

    public override void PointerClick()
    {
        base.PointerClick();
        this.gameObject.SetActive(false);
        AudioManager.instance.Play("AnimationStart");
        pipetteSupernatantAnimator.SetBool("isSupernatantActive", true);
        pcrTubeSupernatantAnimator.SetBool("isSupernatantActive", true);
        AudioManager.instance.Play("Pipette");
        coroutineManager.BeginWaitCoroutine(() =>
        {
            if (puzzleSystemExtraction.isBufferSuccessful == true || puzzleSystemExtraction.isBufferSuccessful == false)
            {
                // Enable Pipette Cry Repeat Game Objects and buttons
                HideExtractionTools();
                //nextSetTools.SetActive(true);
                //nextToolBtn.SetActive(true);
            }
            pipetteSupernatantAnimator.SetBool("isSupernatantActive", false);
            pcrTubeSupernatantAnimator.SetBool("isSupernatantActive", false);
            PushOutNextTools();
            nextToolBtn.SetActive(true);
        }, 3.5f);
        
        puzzleSystemExtraction.PutInIngredient(PuzzleSystemExtraction.IngredientType.Supernatant);
    }

    public void HideExtractionTools()
    {
        for (int i = 0; i < currentTools.Length; i++)
        {
            GatewayPortalManager portalManager = currentTools[i].GetComponent<GatewayPortalManager>();
            //currentTools[i].SetActive(false);
            portalManager.PullIn();
        }
    }

    public void PushOutNextTools()
    {
        coroutineManager.BeginWaitCoroutine(() =>
        {
            for (int i = 0; i < nextSetTools.Length; i++)
            {              
                nextSetTools[i].SetActive(true);
                GatewayPortalManager portalManager = nextSetTools[i].GetComponent<GatewayPortalManager>();
                portalManager.PushOut();             
            }
        }, 2f);       
    }
}
