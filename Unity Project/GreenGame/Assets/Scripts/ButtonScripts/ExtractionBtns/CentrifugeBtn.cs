using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CentrifugeBtn : SpecialSelectionButton
{
    [SerializeField]
    PuzzleSystemExtraction puzzleSystemExtraction;

    [SerializeField]
    GameObject nextExtractionButton;

    [SerializeField]
    CoroutineManager coroutineManager;

    [SerializeField]
    Animator centrifugeStart;

    [SerializeField]
    Animator pcrTubeCentrifugeAnimator;

    public override void PointerClick()
    {
        base.PointerClick();
        AudioManager.instance.Play("AnimationStart");
        this.gameObject.SetActive(false);
        centrifugeStart.SetBool("isCentrifugeActive", true);
        pcrTubeCentrifugeAnimator.SetBool("isCentrifugeActive", true);
        coroutineManager.BeginWaitCoroutine(() =>
        {
            AudioManager.instance.Play("MachineHum");
        }, 4f);
        coroutineManager.BeginWaitCoroutine(() =>
        {
            nextExtractionButton.SetActive(true);
            centrifugeStart.SetBool("isCentrifugeActive", false);
            pcrTubeCentrifugeAnimator.SetBool("isCentrifugeActive", false);
        }, 8.5f);
        puzzleSystemExtraction.PutInIngredient(PuzzleSystemExtraction.IngredientType.Centrifuge);
    }
}
