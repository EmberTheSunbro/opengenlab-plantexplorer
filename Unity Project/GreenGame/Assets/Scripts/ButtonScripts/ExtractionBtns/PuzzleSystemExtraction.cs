using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleSystemExtraction : MonoBehaviour
{
    // Enum for storing type of ingredients based on the gameobject
    public enum IngredientType { Empty, MortarPestle, Buffer, Supernatant, Centrifuge }

    IngredientType extractionProcessOne;
    IngredientType extractionProcessTwo;
    IngredientType extractionProcessThree;
    IngredientType extractionProcessFour;

    private bool evaluationComplete = false;

    [SerializeField]
    SlotManager slotManager;

    private int pumpBufferAmount;
    public int plantSelected;
    private int[] primerPcrStage; // 1 - FORWARD WT cpl1 PRIMER, 2 - FORWARD cpl1-7 PRIMER, 3 - COMMON REVERSE cpl1 PRIMER
    private int[] primerSeqStage; // 1 - FORWARD WT cpl1 PRIMER, 2 - FORWARD cpl1-7 PRIMER, 3 - COMMON REVERSE cpl1 PRIMER

    public bool isBufferSuccessful = false;
    public bool isPCRPrimerSuccessful = false;
    public bool isSeqPrimerSuccessful = false;

    CheckpointManager cm;

    private void Start()
    {
        primerPcrStage = new int[4];
        primerSeqStage = new int[4];
        cm = GameManager.instance.checkpointManager;
    }

    private void Update()
    {       
        CheckBufferAmount();
        //CheckPrimerStage();
    }


    // Function to put in ingredients
    public void PutInIngredient(IngredientType ingredient)
    {
        if (extractionProcessOne == IngredientType.Empty)
        {
            extractionProcessOne = ingredient;
        }
        else if (extractionProcessTwo == IngredientType.Empty)
        {
            extractionProcessTwo = ingredient;
        }
        else if (extractionProcessThree == IngredientType.Empty)
        {
            extractionProcessThree = ingredient;
        }
        else if (extractionProcessFour == IngredientType.Empty)
        {
            extractionProcessFour = ingredient;
        }
    }

    // Checking for right ingredients
    private bool CheckIngredient()
    {
        evaluationComplete = true;

        if (extractionProcessOne != IngredientType.MortarPestle) return false;
        if (extractionProcessTwo != IngredientType.Buffer) return false;
        if (extractionProcessThree != IngredientType.Centrifuge) return false;
        if (extractionProcessFour != IngredientType.Supernatant) return false;

        return true;
    }

    // Called by buttons to set the buffer amount
    public void SetBufferLevel(int _pumpBufferAmount)
    {
        pumpBufferAmount = _pumpBufferAmount;
    }

    // Function to set the primers 
    public void SetPrimerPCRStage(int _primerStage)
    {
        for (int i = 0; i < primerPcrStage.Length; i++)
        {
            if (primerPcrStage[i] == 0)
            {
                primerPcrStage[i] = _primerStage;
                return;
            }
        }
    }

    // Resets primer stages after verification
    public void ResetPrimerPCRStage()
    {
        primerPcrStage = new int[4];
        //primerStage[0] = 0;
        //primerStage[1] = 0;
    }

    // Called by buttons to set the plant selected
    public void SetPlantSelected(int _plantSelected)
    {
        // 1 - Plant Sample A (R only), 2 - Plant Sample B (R+r), Plant Sample C (r only)

        plantSelected = _plantSelected;
    }

    // Function to check primers - PCR stage - called in the "Run Thermal Cycler"
    public void CheckPrimerPCRStage()
    {
        // R only
        if ((plantSelected == 1) && ((primerPcrStage[0] == 1 && primerPcrStage[1] == 3) || (primerPcrStage[0] == 3 && primerPcrStage[1] == 1)) &&
                (primerPcrStage[2] == 0))
        {
            // PCR Primer Stage Required Input - Wild type + common reverse (Any order)
            slotManager.checkpoint3 = true;
            cm.currentCheckpoint = 3;
            isPCRPrimerSuccessful = true;
            Debug.Log("R only");
        }
        else if ((plantSelected == 2) && ((primerPcrStage[0] == 1 || primerPcrStage[0] == 2 || primerPcrStage[0] == 3) &&
                  (primerPcrStage[1] == 1 || primerPcrStage[1] == 2 || primerPcrStage[1] == 3) &&
                  (primerPcrStage[2] == 1 || primerPcrStage[2] == 2 || primerPcrStage[2] == 3)))
        {
            // R + r
            //PCR Primer Stage Required Input - Add all three primers(Any order)
            slotManager.checkpoint3 = true;
            cm.currentCheckpoint = 3;
            isPCRPrimerSuccessful = true;
            Debug.Log("R + r");
        }
        else if ((plantSelected == 3) && ((primerPcrStage[0] == 2 && primerPcrStage[1] == 3) || (primerPcrStage[0] == 3 && primerPcrStage[1] == 2)) && 
                (primerPcrStage[2] == 0))
        {
            // r only
            //PCR Primer Stage Required Input - Cp11-7 and common reverse (Any order)
            slotManager.checkpoint3 = true;
            cm.currentCheckpoint = 3;
            isPCRPrimerSuccessful = true;
            Debug.Log("r only");
        }
        else
        {
            slotManager.checkpoint3 = false;
            isPCRPrimerSuccessful = false;
            ResetPrimerPCRStage();
            Debug.Log("reset");
        }
    }

    // Checks buffer ammount and sets bool
    void CheckBufferAmount()
    {
        if (pumpBufferAmount == 1)
        {
            // Wrong amount
            //Debug.Log("Wrong Amount");
            slotManager.checkpoint2 = false;
            isBufferSuccessful = false;
        }
        else if (pumpBufferAmount == 2)
        {
            // Right amount
            //Debug.Log("Right Amount");
            cm.currentCheckpoint = 2;
            slotManager.checkpoint2 = true;
            isBufferSuccessful = true;
        }
        else
        {
            // Not right
            slotManager.checkpoint2 = false;
            isBufferSuccessful = false;
        }
    }

    // Function that is called by buttons to set the stages
    public void SetPrimerSeqStage(int _primerSeqStage)
    {
        for (int i = 0; i < primerSeqStage.Length; i++)
        {
            if (primerSeqStage[i] == 0)
            {
                primerSeqStage[i] = _primerSeqStage;
                return;
            }
        }
    }

    // Resets the stage after verfication
    public void ResetPrimerSeqStage()
    {
        primerSeqStage = new int[4];
        //primerStage[0] = 0;
        //primerStage[1] = 0;
    }

    // Function to check the primers at the sequencing change - called at the final button
    public void CheckPrimerSeqStage()
    {
        if ((plantSelected == 1) && (primerSeqStage[0] == 1 && primerSeqStage[1] == 0 && primerSeqStage[2] == 0))
        {
            // Sequencing Primer Stage Required Input - Wild type forward primer (Any order)
            slotManager.checkpoint4 = true;
            isSeqPrimerSuccessful = true;
            Debug.Log("R only seq");
        }
        else if ((plantSelected == 2) && ((primerSeqStage[0] == 1 && primerSeqStage[1] == 2) || (primerSeqStage[0] == 2 && primerSeqStage[1] == 1)) 
                    && (primerSeqStage[2] == 0))
        {
            // R + r
            // Sequencing Primer Stage Required Input - Wild type and cp-117 (Any order)
            slotManager.checkpoint4 = true;
            isSeqPrimerSuccessful = true;
            Debug.Log("R + r seq");
        }
        else if ((plantSelected == 3) && (primerSeqStage[0] == 2 && primerSeqStage[1] == 0 && primerSeqStage[2] == 0))
        {
            // r only
            // Sequencing Primer Stage Required Input - cp11-7 only
            slotManager.checkpoint4 = true;
            isSeqPrimerSuccessful = true;
            Debug.Log("r only seq");
        }
        else
        {
            slotManager.checkpoint4 = false;
            isSeqPrimerSuccessful = false;
            ResetPrimerSeqStage();
            Debug.Log("reset");
        }
    }
}
