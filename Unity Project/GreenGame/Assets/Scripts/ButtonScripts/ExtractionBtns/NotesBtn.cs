using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotesBtn : SpecialSelectionButton
{
    [SerializeField]
    private SlotManager slotManager;

    [SerializeField]
    Animator animator;

    [SerializeField]
    GameObject notesAnim;

    [SerializeField]
    GameObject closeBtm;

    public override void PointerClick()
    {
        base.PointerClick();
        gameObject.SetActive(false);
        notesAnim.SetActive(true);
        closeBtm.SetActive(true);
        animator.SetBool("Appear", true);
        //slotManager.ShowIngredientButtons();
    }

}
