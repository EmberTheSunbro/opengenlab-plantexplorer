using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantSampleC : SpecialSelectionButton
{
    // Plant Sample A is r/r which is "r only"
    // PCR Primer Stage Required Input - Cp11-7 and common reverse (Any order)
    // Sequencing Primer Stage Required Input - cp11-7 only


    [SerializeField]
    PuzzleSystemExtraction puzzleSystemExtraction;

    [SerializeField]
    GameObject[] plantSampleButtons;

    [SerializeField]
    GameObject nextExtractionButton;

    public override void PointerClick()
    {
        base.PointerClick();
        AudioManager.instance.Play("AnimationStart");
        this.gameObject.SetActive(false);
        HidePlantSampleButtons();
        nextExtractionButton.SetActive(true);
        puzzleSystemExtraction.SetPlantSelected(3);
    }

    void HidePlantSampleButtons()
    {
        for (int i = 0; i < plantSampleButtons.Length; i++)
        {
            plantSampleButtons[i].SetActive(false);
        }
    }
}
