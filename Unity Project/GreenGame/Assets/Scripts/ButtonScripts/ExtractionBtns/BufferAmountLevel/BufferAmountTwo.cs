using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BufferAmountTwo : SpecialSelectionButton
{
    [SerializeField]
    PuzzleSystemExtraction puzzleSystemExtraction;

    [SerializeField]
    GameObject[] bufferAmountButtons;

    [SerializeField]
    GameObject nextExtractionButton;

    [SerializeField]
    CoroutineManager coroutineManager;

    [SerializeField]
    Animator bufferOpeningAnimtor;

    [SerializeField]
    Animator pipetteBuffer;

    [SerializeField]
    Animator pcrBufferAnimator;

    public override void PointerClick()
    {
        base.PointerClick();
        AudioManager.instance.Play("AnimationStart");
        this.gameObject.SetActive(false);
        bufferOpeningAnimtor.SetBool("isBufferActive", true);
        pipetteBuffer.SetBool("isBufferActive", true);
        pcrBufferAnimator.SetBool("isBufferActive", true);
        HideBufferButtons();
        coroutineManager.BeginWaitCoroutine(() =>
        {
            AudioManager.instance.Play("Pipette");
        }, 2.5f);
        coroutineManager.BeginWaitCoroutine(() =>
        {
            nextExtractionButton.SetActive(true);
            bufferOpeningAnimtor.SetBool("isBufferActive", false);
            pipetteBuffer.SetBool("isBufferActive", false);
            pcrBufferAnimator.SetBool("isBufferActive", false);
        }, 8f);
        puzzleSystemExtraction.SetBufferLevel(2);      
    }

    void HideBufferButtons()
    {
        for (int i = 0; i < bufferAmountButtons.Length; i++)
        {
            bufferAmountButtons[i].SetActive(false);
        }
    }
}
