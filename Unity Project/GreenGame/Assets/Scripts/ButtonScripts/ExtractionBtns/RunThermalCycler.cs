using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunThermalCycler : SpecialSelectionButton
{
    [SerializeField]
    PuzzleSystemExtraction puzzleSystemExtraction;

    [SerializeField]
    SlotManager slotManager;

    [SerializeField]
    GameObject[] pcrTools;

    [SerializeField]
    GameObject[] ingredientButtons;

    [SerializeField]
    VideoController videoController;

    [SerializeField]
    DNARevealer revealer;

    [SerializeField]
    GameObject cyclingObject;

    [SerializeField]
    CoroutineManager coroutineManager;

    [SerializeField]
    GameObject proceedSeqBtn;

    [SerializeField]
    Animator pcrTube;

    [SerializeField]
    Animator thermalCycler;

    public override void PointerClick()
    {
        base.PointerClick();
        this.gameObject.SetActive(false);
        CheckProgress();
        // Starts cycling and starts playing video on screen
    }

    //Function to check the progress for buffer amount and pcr primers
    void CheckProgress()
    {
        if ((slotManager.checkpoint1 == true && slotManager.checkpoint2 == false && slotManager.checkpoint3 == false) || 
            (slotManager.checkpoint1 == true && slotManager.checkpoint2 == false && slotManager.checkpoint3 == true))
        {
            // Puts the player back to the step after sorting (BUFFER)
            //slotManager.HideIngredientButtons();
            AudioManager.instance.Play("Incorrect");
            HidePCRObjects();
            slotManager.PutOrganizationToolsBack();
            puzzleSystemExtraction.ResetPrimerPCRStage();
            slotManager.ChangeTextForStage(slotManager.textMeshPro, "Incorrect Buffer Amount. Let's try again.");
            coroutineManager.BeginWaitCoroutine(() =>
            {
                slotManager.ChangeTextForStage(slotManager.textMeshPro, "");
                slotManager.logo.SetActive(true);
            }, 6f);
        }
        else if (slotManager.checkpoint1 == true && slotManager.checkpoint2 == true && slotManager.checkpoint3 == false)
        {
            // Puts the Player to the PCR steps
            AudioManager.instance.Play("Incorrect");
            slotManager.PutPCRToolsBack();
            puzzleSystemExtraction.ResetPrimerPCRStage();
            slotManager.ChangeTextForStage(slotManager.textMeshPro, "Correct Buffer Amount! Primers Wrong. Let's try again");

            coroutineManager.BeginWaitCoroutine(() =>
            {
                slotManager.ChangeTextForStage(slotManager.textMeshPro, "");
                slotManager.logo.SetActive(true);
            }, 6f);
        }
        else if (slotManager.checkpoint1 == true && slotManager.checkpoint2 == true && slotManager.checkpoint3 == true)
        {
            // Runs the thermo cycler successfully
            AudioManager.instance.Play("Correct");
            slotManager.ChangeTextForStage(slotManager.textMeshPro, "Correct Buffer Amount and Primers! Success!");
            pcrTube.SetBool("isPCRSelected", true);
            thermalCycler.SetBool("isPCRSelected", true);

            coroutineManager.BeginWaitCoroutine(() =>
            {
                pcrTube.SetBool("isPCRSelected", false);
                thermalCycler.SetBool("isPCRSelected", false);
                PlayDNACycling();
            }, 2.5f);

            coroutineManager.BeginWaitCoroutine(() =>
            {
                slotManager.ChangeTextForStage(slotManager.textMeshPro, "");
                slotManager.logo.SetActive(true);
            }, 6f);
            // Enable Sequencing Button and Tools
        }
        Debug.Log(slotManager.checkpoint1);
    }

    public void HidePCRObjects()
    {
        for (int i = 0; i < pcrTools.Length; i++)
        {
            GatewayPortalManager portalManager = pcrTools[i].GetComponent<GatewayPortalManager>();
            //currentTools[i].SetActive(false);        
            portalManager.PullIn();
        }

    }

    void PushOutNextTools()
    {
        for (int i = 0; i < pcrTools.Length; i++)
        {
            GatewayPortalManager portalManager = pcrTools[i].GetComponent<GatewayPortalManager>();
            //currentTools[i].SetActive(false);
            portalManager.PullIn();
        }
    }

    void HideIngredientButtons()
    {
        for (int i = 0; i < ingredientButtons.Length; i++)
        {
            ingredientButtons[i].SetActive(false);
        }
    }

    // Function to trigger video playing
    void PlayDNACycling()
    {
        // Plays the video on the screen and cycles the dna
        AudioManager.instance.Play("MachineHum");
        AudioManager.instance.Play("ProcessingSFX"); 
        cyclingObject.SetActive(true);
        videoController.isVideoPlaying = true;
        videoController.CheckVideoPLaying();
        revealer.isDNARevealing = true;
        this.gameObject.SetActive(true);
        coroutineManager.BeginWaitCoroutine(() => {
            videoController.isVideoPlaying = false;
            videoController.CheckVideoPLaying();
            revealer.isDNARevealing = false;
            cyclingObject.SetActive(false);
            HidePCRObjects();
            slotManager.attentionRing.SetActive(true);
            proceedSeqBtn.SetActive(true);
        }, 10f);
        //StartCoroutine(coroutineManager.WaitCoroutine(() => {
        //    Debug.Log("Deactivated");
        //    videoController.isVideoPlaying = false;
        //    revealer.isDNARevealing = false;
        //    cyclingObject.SetActive(false);
        //}, 10f));
        this.gameObject.SetActive(false);        
    }
  
}
