using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantSampleA : SpecialSelectionButton
{
    // Plant Sample A is R/R which is "R only"
    // PCR Primer Stage Required Input - Wild type + common reverse (Any order)
    // Sequencing Primer Stage Required Input - Wild type forward primer (Any order)

    [SerializeField]
    PuzzleSystemExtraction puzzleSystemExtraction;

    [SerializeField]
    GameObject[] plantSampleButtons;

    [SerializeField]
    GameObject nextExtractionButton;

    public override void PointerClick()
    {
        base.PointerClick();
        AudioManager.instance.Play("AnimationStart");
        this.gameObject.SetActive(false);
        HidePlantSampleButtons();
        nextExtractionButton.SetActive(true);
        puzzleSystemExtraction.SetPlantSelected(1);
    }

    void HidePlantSampleButtons()
    {
        for (int i = 0; i < plantSampleButtons.Length; i++)
        {
            plantSampleButtons[i].SetActive(false);
        }
    }
}
