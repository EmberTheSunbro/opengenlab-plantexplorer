using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantSampleB : SpecialSelectionButton
{
    // Plant Sample A is R/r which is "R + r"
    // PCR Primer Stage Required Input - Add all three primers (Any order)
    // Sequencing Primer Stage Required Input - Wild type and cp-117 (Any order)


    [SerializeField]
    PuzzleSystemExtraction puzzleSystemExtraction;

    [SerializeField]
    GameObject[] plantSampleButtons;

    [SerializeField]
    GameObject nextExtractionButton;

    public override void PointerClick()
    {
        base.PointerClick();
        AudioManager.instance.Play("AnimationStart");
        this.gameObject.SetActive(false);
        HidePlantSampleButtons();
        nextExtractionButton.SetActive(true);
        puzzleSystemExtraction.SetPlantSelected(2);
    }

    void HidePlantSampleButtons()
    {
        for (int i = 0; i < plantSampleButtons.Length; i++)
        {
            plantSampleButtons[i].SetActive(false);
        }
    }
}
