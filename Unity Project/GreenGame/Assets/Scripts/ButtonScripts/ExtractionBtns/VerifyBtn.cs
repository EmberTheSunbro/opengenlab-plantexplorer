using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerifyBtn : SpecialSelectionButton
{
    [SerializeField]
    private SlotManager slotManager;

    public GameObject[] ingredientButtons;

    public GameObject extractionButton;

    [SerializeField]
    private GameObject thisButton;

    public override void PointerClick()
    {
        base.PointerClick();
        thisButton.SetActive(false);
        slotManager.HandleSlotsFull();
        //HideIngredientButtons();
        // Checks for correct order
        //ShowExtractionButtons();
        // If correct order, shows up the puzzle extraction buttons else randomize again

    }

    public void HideIngredientButtons()
    {
        for (int i = 0; i < ingredientButtons.Length; i++)
        {
            ingredientButtons[i].SetActive(false);
        }
    }

    public void ShowExtractionButtons()
    {        
        extractionButton.SetActive(true);
    }

    public void HideExtractionButtons()
    {
        // Setting Plant Sample buttons to false
        extractionButton.SetActive(false);
    }
}
