using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddBufferBtn : SpecialSelectionButton
{
    [SerializeField]
    PuzzleSystemExtraction puzzleSystemExtraction;

    public override void PointerClick()
    {
        base.PointerClick();
        puzzleSystemExtraction.PutInIngredient(PuzzleSystemExtraction.IngredientType.Buffer);
    }
}
