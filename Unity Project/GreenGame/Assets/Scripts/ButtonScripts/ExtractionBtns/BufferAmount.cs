using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BufferAmount : SpecialSelectionButton
{
    [SerializeField]
    PuzzleSystemExtraction puzzleSystemExtraction;

    [SerializeField]
    private int bufferAmount;

    public override void PointerClick()
    {
        base.PointerClick();
        puzzleSystemExtraction.SetBufferLevel(bufferAmount);
    }
}
