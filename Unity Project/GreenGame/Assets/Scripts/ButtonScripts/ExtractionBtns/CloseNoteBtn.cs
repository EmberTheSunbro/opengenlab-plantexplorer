using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CloseNoteBtn : SpecialSelectionButton
{
    [SerializeField]
    GameObject notesButton;

    [SerializeField]
    Animator animator;

    public override void PointerClick()
    {
        base.PointerClick();
        gameObject.SetActive(false);
        animator.SetBool("Appear", false);
        notesButton.SetActive(true);
        //slotManager.ShowIngredientButtons();
    }
}
