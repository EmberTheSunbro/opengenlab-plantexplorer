using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MicroscopeButton : SpecialSelectionButton
{

    [SerializeField]
    Menu menu;

    public override void PointerClick()
    {
        base.PointerClick();

        menu.StartCoroutine("MicroScopeTransition");
    }
}
