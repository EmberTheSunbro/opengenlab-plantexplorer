using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public abstract class SpecialSelectionButton : MonoBehaviour
   //,IPointerClickHandler
{

    [System.NonSerialized] public bool isLerping = false; //prevents lerping again if already lerping 

    Coroutine lastLerpCoroutine;

    [SerializeField]
    private float timeToLerp = 1f;
   
    public Image myImage;

    [SerializeField]
    protected Image highlightedImage;

    [SerializeField]
    Sprite highlightedSprite;

    [SerializeField]
    Sprite neutralSprite;

    public Canvas worldspaceCanvas;

    private bool isReceeding;

    bool receedingFunctionalityToggle = false;

    protected virtual void Awake()
    {
        myImage = GetComponent<Image>();
        //Debug.Log(myImage + " " + this.gameObject.name);
    }

    public virtual void PointerClick()
    {
        AudioManager.instance.StopPlaying("Fill");
    }

    /*public virtual void OnPointerClick(PointerEventData eventData)
    {
        
    }*/

    public virtual void PointerEnter()
    {
        //Debug.Log(myImage);
        //myImage.sprite = highlightedSprite;

        AudioManager.instance.Play("Fill");

        if (!isLerping && highlightedImage.gameObject.activeInHierarchy)
        {
            isLerping = true;
            if (lastLerpCoroutine != null) StopCoroutine(lastLerpCoroutine);
            lastLerpCoroutine = StartCoroutine(LerpCoroutine(0f, 1f, false));
        }
    }

    public virtual void PointerExit()
    {
        //Debug.Log("Exit");;
        //myImage.sprite = neutralSprite;

        AudioManager.instance.StopPlaying("Fill");

        if (isLerping)
        {
            isLerping = false;

            if (lastLerpCoroutine != null) StopCoroutine(lastLerpCoroutine);
        }
        if (!isReceeding && highlightedImage.gameObject.activeInHierarchy && receedingFunctionalityToggle)
        {
            isReceeding = true;
            lastLerpCoroutine = StartCoroutine(LerpCoroutine(1f, 0f, false));
        }
        else if (!receedingFunctionalityToggle) //If we don't want it to receed slowly
        {
            if (highlightedImage.type == Image.Type.Filled) //For fillable types
            {
                highlightedImage.fillAmount = 0;
            }
            else //For other image types
            {
                Color tempColor = highlightedImage.color;
                tempColor.a = 0;
                highlightedImage.color = tempColor;
            }
        }
    }

    public IEnumerator LerpCoroutine(float startAlpha, float endAlpha, bool disableOnFinish)
    {
        float lerpTimer = 0f;
        float lerpTime = timeToLerp;

        while (lerpTimer < lerpTime)
        {
            lerpTimer += Time.deltaTime;
            //Debug.Log(lerpTimer);
            if (lerpTimer > lerpTime)
            {
                lerpTimer = lerpTime;
            }

            // Lerp math
            float t = lerpTimer / lerpTime;
            t = Mathf.Sin(t * Mathf.PI * 0.5f);

            if(highlightedImage.type == Image.Type.Filled) //Function for fillable images
            {
                highlightedImage.fillAmount = Mathf.Lerp(startAlpha, endAlpha, t);
            }
            else //Functionality for images with alpha
            {
                Color tempColor = highlightedImage.color;
                tempColor.a = Mathf.Lerp(startAlpha, endAlpha, t);
                highlightedImage.color = tempColor;
            }
            
            yield return null;
        }
        isLerping = false;
        isReceeding = false;

        if(highlightedImage.type == Image.Type.Filled) //For fillable types
        {
            if (highlightedImage.fillAmount == 1)
            {
                PointerClick();
            }
        }
        else
        {
            if (highlightedImage.color.a == 1) //For other image types
            {
                PointerClick();
            }
        }

        
    }

    
}
