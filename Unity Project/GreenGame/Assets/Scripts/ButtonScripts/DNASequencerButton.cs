using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class DNASequencerButton : SpecialSelectionButton
{
    /*public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);
        SceneLoader.instance.LoadSequencerScene();
    }*/

    public override void PointerClick()
    {
        base.PointerClick();
        GameManager.instance.sceneLoader.LoadSequencerScene();
    }

}
