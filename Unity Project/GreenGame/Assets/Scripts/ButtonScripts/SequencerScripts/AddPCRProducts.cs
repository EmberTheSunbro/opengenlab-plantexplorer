using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddPCRProducts : SpecialSelectionButton
{
    [SerializeField]
    GameObject[] nextSeqBtn;

    [SerializeField]
    CoroutineManager coroutineManager;

    [SerializeField]
    Animator pcrTubeAddProduct;

    [SerializeField]
    Animator pipetteAddProducts;

    public override void PointerClick()
    {
        base.PointerClick();
        AudioManager.instance.Play("AnimationStart");
        AudioManager.instance.Play("Pipette");
        this.gameObject.SetActive(false);

        pcrTubeAddProduct.SetBool("isAddingProducts", true);
        pipetteAddProducts.SetBool("isAddingProducts", true);

        coroutineManager.BeginWaitCoroutine(() => {
            pcrTubeAddProduct.SetBool("isAddingProducts", false);
            pipetteAddProducts.SetBool("isAddingProducts", false);
            ShowNextButtons();
        }, 3.5f);
        
    }

    public void ShowNextButtons()
    {
        for (int i = 0; i < nextSeqBtn.Length; i++)
        {
            nextSeqBtn[i].SetActive(true);
        }
    }
}
