using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NanoporeSeqBtn : SpecialSelectionButton
{
    [SerializeField]
    GameObject nextSeqBtn;

    [SerializeField]
    CoroutineManager coroutineManager;

    public override void PointerClick()
    {
        base.PointerClick();
        AudioManager.instance.Play("MachineHum");
        this.gameObject.SetActive(false);
        coroutineManager.BeginWaitCoroutine(() => {
            nextSeqBtn.SetActive(true);
        }, 2f);
    }
}
