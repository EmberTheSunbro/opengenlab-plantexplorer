using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotorAdaptorBtn : SpecialSelectionButton
{
    [SerializeField]
    GameObject nextSeqBtn;

    [SerializeField]
    CoroutineManager coroutineManager;

    [SerializeField]
    Animator pipetteMotor;

    [SerializeField]
    Animator SequencerMotor;

    public override void PointerClick()
    {
        base.PointerClick();
        AudioManager.instance.Play("AnimationStart");
        this.gameObject.SetActive(false);

        pipetteMotor.SetBool("isMotorAdaptor", true);
        SequencerMotor.SetBool("isMotorAdaptor", true);

        coroutineManager.BeginWaitCoroutine(() => {
            pipetteMotor.SetBool("isMotorAdaptor", false);
            SequencerMotor.SetBool("isMotorAdaptor", false);
            nextSeqBtn.SetActive(true);
        }, 4f);
    }
}
