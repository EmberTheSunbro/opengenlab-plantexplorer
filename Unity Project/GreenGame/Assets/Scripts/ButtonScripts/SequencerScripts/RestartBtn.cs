using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartBtn : SpecialSelectionButton
{

    public override void PointerClick()
    {
        GameManager.instance.sceneLoader.LoadThermalCyclerScene();
    }


}
