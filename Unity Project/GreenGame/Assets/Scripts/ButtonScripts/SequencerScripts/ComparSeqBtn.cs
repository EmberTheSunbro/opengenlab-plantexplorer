using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComparSeqBtn : SpecialSelectionButton
{
    [SerializeField]
    PuzzleSystemExtraction puzzleSystemExtraction;

    [SerializeField]
    SlotManager slotManager;

    [SerializeField]
    GameObject nextSeqBtn;

    [SerializeField]
    GameObject previousSeqBtn;

    [SerializeField]
    CoroutineManager coroutineManager;

    [SerializeField]
    Animator dnaUnrollingPlantSampleAAnimator;

    [SerializeField]
    GameObject dnaUnrollingObjectPlantSampleA;

    [SerializeField]
    Animator dnaUnrollingPlantSampleBAnimator;

    [SerializeField]
    GameObject dnaUnrollingObjectPlantSampleB;

    [SerializeField]
    Animator dnaUnrollingPlantSampleCAnimator;

    [SerializeField]
    GameObject dnaUnrollingObjectPlantSampleC;

    [SerializeField]
    FinalMonitorLerp finalMonitorLerp;


    public override void PointerClick()
    {
        base.PointerClick();
        finalMonitorLerp.originalMonitorPos = finalMonitorLerp.monitorLerpStartPos.transform.position; // Getting reference to local position
        this.gameObject.SetActive(false);
        puzzleSystemExtraction.CheckPrimerSeqStage();
        CheckPrimerProgress();
    }
    
    // Checks Primers, Moves Montior and Changes Text on Monitor
    void CheckPrimerProgress()
    {
        if (puzzleSystemExtraction.plantSelected == 1 && slotManager.checkpoint4 == true)
        {
            finalMonitorLerp.monitorLerpStartPos.rotation = Quaternion.Euler(0, 0, 0);
            slotManager.ChangeTextForStage(slotManager.textMeshPro, "Correct Primers! Study the sequence.");
            AudioManager.instance.Play("Correct");
            AudioManager.instance.Play("ProcessingSFX");
            dnaUnrollingObjectPlantSampleA.SetActive(true);
            dnaUnrollingPlantSampleAAnimator.SetBool("isUnrolling", true);
            finalMonitorLerp.MoveMonitorToSuccessPosition();

            coroutineManager.BeginWaitCoroutine(() => {
                nextSeqBtn.SetActive(true);
            }, 10f);

            coroutineManager.BeginWaitCoroutine(() =>
            {
                slotManager.ChangeTextForStage(slotManager.textMeshPro, "");
                slotManager.logo.SetActive(true);
            }, 20f);
        }
        else if (puzzleSystemExtraction.plantSelected == 2 && slotManager.checkpoint4 == true)
        {
            finalMonitorLerp.monitorLerpStartPos.rotation = Quaternion.Euler(0, 0, 0);
            slotManager.ChangeTextForStage(slotManager.textMeshPro, "Correct Primers! Study the sequence.");
            AudioManager.instance.Play("Correct");
            AudioManager.instance.Play("ProcessingSFX");
            dnaUnrollingObjectPlantSampleB.SetActive(true);
            dnaUnrollingPlantSampleBAnimator.SetBool("isUnrolling", true);
            finalMonitorLerp.MoveMonitorToSuccessPosition();

            coroutineManager.BeginWaitCoroutine(() => {
                nextSeqBtn.SetActive(true);
            }, 10f);

            coroutineManager.BeginWaitCoroutine(() =>
            {
                slotManager.ChangeTextForStage(slotManager.textMeshPro, "");
                slotManager.logo.SetActive(true);
            }, 20f);
        }
        else if (puzzleSystemExtraction.plantSelected == 3 && slotManager.checkpoint4 == true)
        {
            finalMonitorLerp.monitorLerpStartPos.rotation = Quaternion.Euler(0, 0, 0);
            slotManager.ChangeTextForStage(slotManager.textMeshPro, "Correct Primers! Study the sequence.");
            AudioManager.instance.Play("Correct");
            AudioManager.instance.Play("ProcessingSFX");
            dnaUnrollingObjectPlantSampleC.SetActive(true);
            dnaUnrollingPlantSampleCAnimator.SetBool("isUnrolling", true);
            finalMonitorLerp.MoveMonitorToSuccessPosition();

            coroutineManager.BeginWaitCoroutine(() => {
                nextSeqBtn.SetActive(true);
            }, 10f);

            coroutineManager.BeginWaitCoroutine(() =>
            {
                slotManager.ChangeTextForStage(slotManager.textMeshPro, "");
                slotManager.logo.SetActive(true);
            }, 20f);
        }
        else 
        {        
            slotManager.ChangeTextForStage(slotManager.textMeshPro, "Incorrect Primers. Let's try again.");
            AudioManager.instance.Play("Incorrect");
            coroutineManager.BeginWaitCoroutine(() => {
                puzzleSystemExtraction.ResetPrimerSeqStage();
                previousSeqBtn.SetActive(true);
            }, 4f);

            coroutineManager.BeginWaitCoroutine(() =>
            {
                slotManager.ChangeTextForStage(slotManager.textMeshPro, "");
                slotManager.logo.SetActive(true);
            }, 8f);
        }
    }
    
}
