using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeqForwardPrimerTwo : SpecialSelectionButton
{
    [SerializeField]
    PuzzleSystemExtraction puzzleSystemExtraction;

    [SerializeField]
    SlotManager SlotManager;

    public override void PointerClick()
    {
        base.PointerClick();
        AudioManager.instance.Play("AnimationStart");
        this.gameObject.SetActive(false);
        puzzleSystemExtraction.SetPrimerSeqStage(2);
    }
}
