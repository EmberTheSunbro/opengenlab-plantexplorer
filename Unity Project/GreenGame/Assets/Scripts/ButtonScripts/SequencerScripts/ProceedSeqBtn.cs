using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceedSeqBtn : SpecialSelectionButton
{
    [SerializeField]
    SlotManager slotManager;

    public GameObject nextSeqBtn;

    [SerializeField]
    GameObject[] SequencingTools;

    [SerializeField]
    CoroutineManager coroutineManager;

    public override void PointerClick()
    {
        base.PointerClick();
        slotManager.attentionRing.SetActive(false);
        AudioManager.instance.Play("AnimationStart");
        this.gameObject.SetActive(false);
        nextSeqBtn.SetActive(true);
        ShowSequencingTools();
    }

    public void ShowSequencingTools()
    {
        coroutineManager.BeginWaitCoroutine(() =>
        {
            for (int i = 0; i < SequencingTools.Length; i++)
            {
                SequencingTools[i].SetActive(true);
                GatewayPortalManager portalManager = SequencingTools[i].GetComponent<GatewayPortalManager>();
                portalManager.PushOut();
                slotManager.ChangeTextForStage(slotManager.textMeshPro, "Let's start Sequencing!");
            }
        }, 1.5f);

        coroutineManager.BeginWaitCoroutine(() =>
        {
            slotManager.ChangeTextForStage(slotManager.textMeshPro, "");
            slotManager.logo.SetActive(true);
        }, 6f);
    }
}

