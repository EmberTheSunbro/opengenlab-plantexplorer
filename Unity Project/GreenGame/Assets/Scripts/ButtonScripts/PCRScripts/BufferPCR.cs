using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BufferPCR : SpecialSelectionButton
{
    [SerializeField]
    GameObject nextButton;

    [SerializeField]
    CoroutineManager coroutineManager;

    [SerializeField]
    Animator pcrTubeBuffer;

    [SerializeField]
    Animator pcrPipetteBuffer;

    [SerializeField]
    Animator bufferOpening;


    public override void PointerClick()
    {
        base.PointerClick();
        AudioManager.instance.Play("AnimationStart");
        this.gameObject.SetActive(false);
        coroutineManager.BeginWaitCoroutine(() =>
        {
            AudioManager.instance.Play("Pipette");
        }, 1f);
        
        pcrTubeBuffer.SetBool("isPCRBuffer", true);
        pcrPipetteBuffer.SetBool("isPCRBuffer", true);
        bufferOpening.SetBool("isBufferActive", true);

        coroutineManager.BeginWaitCoroutine(() =>
        {
            pcrTubeBuffer.SetBool("isPCRBuffer", false);
            pcrPipetteBuffer.SetBool("isPCRBuffer", false);
            bufferOpening.SetBool("isBufferActive", false);

            nextButton.SetActive(true);
        }, 8f);

        
        // Starts cycling and starts playing video on screen
    }
}

