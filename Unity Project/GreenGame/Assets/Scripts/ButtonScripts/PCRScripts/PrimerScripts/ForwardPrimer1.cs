using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForwardPrimer1 : SpecialSelectionButton
{
    [SerializeField]
    PuzzleSystemExtraction puzzleSystemExtraction;

    [SerializeField]
    SlotManager SlotManager;

    public override void PointerClick()
    {
        base.PointerClick();
        AudioManager.instance.Play("AnimationStart");
        this.gameObject.SetActive(false);
        puzzleSystemExtraction.SetPrimerPCRStage(1);
    }


}
