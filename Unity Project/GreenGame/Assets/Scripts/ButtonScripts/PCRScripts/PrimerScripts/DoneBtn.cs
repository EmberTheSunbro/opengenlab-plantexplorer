using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoneBtn : SpecialSelectionButton
{
    [SerializeField]
    PuzzleSystemExtraction puzzleSystemExtraction;

    [SerializeField]
    SlotManager SlotManager;

    [SerializeField]
    GameObject[] existingObjects;

    [SerializeField]
    GameObject nextPcrBtn;

    public override void PointerClick()
    {
        base.PointerClick();
        AudioManager.instance.Play("AnimationStart");
        this.gameObject.SetActive(false);
        HideExistingButtons();
        nextPcrBtn.SetActive(true);
        puzzleSystemExtraction.CheckPrimerPCRStage();
    }

    public void HideExistingButtons()
    {
        for (int i = 0; i < existingObjects.Length; i++)
        {
            existingObjects[i].SetActive(false);
        }
    }
}
