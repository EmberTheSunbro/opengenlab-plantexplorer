using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReversePrimer2 : SpecialSelectionButton
{
    [SerializeField]
    PuzzleSystemExtraction puzzleSystemExtraction;

    [SerializeField]
    SlotManager SlotManager;

    public override void PointerClick()
    {
        base.PointerClick();
        this.gameObject.SetActive(false);
        puzzleSystemExtraction.SetPrimerPCRStage(4);
    }
}
