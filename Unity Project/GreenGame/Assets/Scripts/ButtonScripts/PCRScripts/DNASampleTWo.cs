using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DNASampleTWo : SpecialSelectionButton
{
    [SerializeField]
    GameObject[] nextPcrBtns;

    [SerializeField]
    CoroutineManager coroutineManager;

    [SerializeField]
    Animator pcrAddDnaAnimator;

    [SerializeField]
    Animator pipetteAddDnaAnimator;

    public override void PointerClick()
    {
        base.PointerClick();
        AudioManager.instance.Play("AnimationStart");
        this.gameObject.SetActive(false);
        coroutineManager.BeginWaitCoroutine(() =>
        {
            AudioManager.instance.Play("Pipette");
        }, 1f);
        pcrAddDnaAnimator.SetBool("isDNASelected", true);
        pipetteAddDnaAnimator.SetBool("isDNASelected", true);

        coroutineManager.BeginWaitCoroutine(() =>
        {           
            pcrAddDnaAnimator.SetBool("isDNASelected", false);
            pipetteAddDnaAnimator.SetBool("isDNASelected", false);
            ShowNextButtons();
        }, 3.5f);
        
        // Starts cycling and starts playing video on screen
    }

    public void ShowNextButtons()
    {
        for (int i = 0; i < nextPcrBtns.Length; i++)
        {
            nextPcrBtns[i].SetActive(true);
        }
    }
}
