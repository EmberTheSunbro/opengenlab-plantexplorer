using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleSlot : MonoBehaviour
{
    public Ingredient storedIngredient;

    //public bool isSelected = false;

    public void SetStoredIngredient(Ingredient _storedIngredient)
    {
        storedIngredient = _storedIngredient;
        if(storedIngredient != null)
        {
            storedIngredient.SetSlot(this);
        }
        
    }

    //public void SetIsSelected(bool state)
    //{
    //    isSelected = state;
    //}
}
