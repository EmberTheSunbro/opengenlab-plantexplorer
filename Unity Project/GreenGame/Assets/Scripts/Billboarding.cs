using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboarding : MonoBehaviour
{
    InputDetection inputDetection;
    private Camera cam;

    private void Start()
    {
        inputDetection = GameManager.instance.inputDetection;
        if (inputDetection.VRMode)
        {
            cam = inputDetection.VRCamera.GetComponent<Camera>();
        }
        else
        {
            cam = inputDetection.DesktopCamera.GetComponent<Camera>();
        }
    }

    private void LateUpdate() 
    {
        //Quaternion rotation = Quaternion.LookRotation(cam.transform.forward, Vector3.up);
        //transform.rotation = rotation;

        transform.LookAt(transform.position * 2 - cam.transform.position);
    }
}
