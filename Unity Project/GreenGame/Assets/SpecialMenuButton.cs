using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpecialMenuButton : SpecialSelectionButton
{
    //Assign text to appear during the onpointer enter
    public Animator hoverPanelTextToActivate;

    [SerializeField]
    Button thisButton;

    public string thisOpenSound;

    [SerializeField]
    Toggle thisToggle;

    public override void PointerClick()
    {

        AudioManager.instance.Play("Open");

        base.PointerClick();
        if (thisButton) //If theres a button click it
        {
            thisButton.onClick.Invoke();
        }

        if (thisToggle)
        {
            if (thisToggle.isOn)
            {
                thisToggle.isOn = false;
            }
            else
            {
                thisToggle.isOn = true;
            }
        }
        
        
    }

    public override void PointerEnter()
    {

        if (hoverPanelTextToActivate)
        {
            hoverPanelTextToActivate.SetTrigger("Appear");
        }
        base.PointerEnter();
    }

    public override void PointerExit()
    {

        if (hoverPanelTextToActivate)
        {
            hoverPanelTextToActivate.SetTrigger("Disappear");
        }
        base.PointerExit();
    }
}
