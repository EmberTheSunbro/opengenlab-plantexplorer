using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DNAMinigameActivation : MonoBehaviour
{
    [SerializeField]
    GameObject infoParent;

    public void ReverseActivation(bool active)
    {
        infoParent.SetActive(!active);
    }

}
