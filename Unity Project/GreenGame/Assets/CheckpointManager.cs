using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointManager : MonoBehaviour
{
    bool checkpoint1, checkpoint2, checkpoint3 = false;

    bool successPoint0, successPoint1, successPoint2, successPoint3 = false;

    public int currentlySelectedCheckpoint = 0;

    public int currentCheckpoint = 0;

    [SerializeField]
    GameObject[] checkpointButtons;

    [SerializeField]
    GameObject alternateCheckpointButtons;

    private int lastUpdatedCheckpoint = 0;

    public bool toolsOrganized = false;

    public bool toolsOut = false;

    private void Update()
    {
    //    if(lastUpdatedCheckpoint != currentCheckpoint)
    //    {           
    //        for (int i = 0; i < currentCheckpoint; i++)
    //        {
    //            checkpointButtons[currentCheckpoint - 1].SetActive(true);
    //        }
    //        lastUpdatedCheckpoint = currentCheckpoint;
    //    }
        if(toolsOrganized == true)
        {
            alternateCheckpointButtons.SetActive(true);
        }

    }

    public void SelectNewCheckpoint(int checkpoint)
    {
        SlotManager tempSlotManager = GameObject.Find("SlotManager").GetComponent<SlotManager>();
        //Store checkpoint bools
        checkpoint1 = tempSlotManager.checkpoint1;
        checkpoint2 = tempSlotManager.checkpoint2;
        checkpoint3 = tempSlotManager.checkpoint3;

        //Store stage success bools to the success steps of buffer amount and primer
        //buffercorrect
        //primer correct
        successPoint0 = tempSlotManager.isSortingSuccessful; // Sorting success
        successPoint1 = tempSlotManager.puzzleSystemExtraction.isBufferSuccessful; // Buffer amount
        successPoint2 = tempSlotManager.puzzleSystemExtraction.isPCRPrimerSuccessful; // PCR Primer success
        successPoint3 = tempSlotManager.puzzleSystemExtraction.isSeqPrimerSuccessful; // Sequencing Primer success


        currentlySelectedCheckpoint = checkpoint;

        GameManager.instance.sceneLoader.LoadThermalCyclerScene();
    }

    public void DisableCheckpointButtons()
    {
        checkpointButtons[1].SetActive(false);
        checkpointButtons[2].SetActive(false);
    }

    public void OrganizeTools()
    {
        //currentCheckpoint = 1;
        toolsOut = true;
        GameManager.instance.sceneLoader.LoadThermalCyclerScene();       
    }
}
