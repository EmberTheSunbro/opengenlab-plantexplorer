using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackupActivation : MonoBehaviour
{
    public SceneActivation one;
    public SceneActivation two;

    bool run;

    private void Update()
    {
        if (!run)
        {
            one.validateObjectActivation(SceneManager.GetActiveScene().name);
            two.validateObjectActivation(SceneManager.GetActiveScene().name);
            run = true;
        }


    }


}
