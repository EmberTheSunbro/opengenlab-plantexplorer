using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public SceneLoader sceneLoader;

    public PopUpManager popUpManager;

    public InputDetection inputDetection;

    public InventoryManager inventoryManager;

    public GameObject gameCamera;

    public EyeRaycaster eyeRaycaster;

    public Canvas playerCanvas;

    public GameObject zoomToggleGroup;

    public GameObject zoomToggleGroupInMenu;

    public GameObject playerRig;

    public GameObject settingsMenu;

    public AudioManager am;

    public Vector3 playerRigDefaultPosition;

    public CanvasGroup blackOverlay;
    public float blinkDuration = 0.5f;

    public bool blackScreenToggle = false;
    public bool screenBlackedOut = false;

    public GameObject mainMenuUI;

    public LayerMask startMask;

    public LightingSettings mainLightSettings;

    public Toggle macroToggle;

    public PopupScript popupOpen;

    public CheckpointManager checkpointManager;

    public bool hasBeenPlayed = false;

    public bool teleportToggle = true;

    private void Awake()
    {
        if(GameManager.instance == null)
        {
            GameManager.instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(gameObject);

        zoomToggleGroup = zoomToggleGroupInMenu;

        gameCamera = inputDetection.VRMode ? inputDetection.VRCamera : inputDetection.DesktopCamera;

        if (checkpointManager == null) checkpointManager = gameObject.GetComponentInChildren<CheckpointManager>();
    }

    private void Start()
    {
        if(sceneLoader == null) sceneLoader = gameObject.GetComponent<SceneLoader>();
        if(inputDetection == null) inputDetection = gameObject.GetComponentInChildren<InputDetection>();
        if (eyeRaycaster == null) eyeRaycaster = gameObject.GetComponentInChildren<EyeRaycaster>();
        if (playerCanvas == null) playerCanvas = gameObject.GetComponentInChildren<Canvas>();
        if (popUpManager == null) popUpManager = gameObject.GetComponentInChildren<PopUpManager>();
        if (inventoryManager == null) inventoryManager = gameObject.GetComponentInChildren<InventoryManager>();
        //if (checkpointManager == null) checkpointManager = gameObject.GetComponentInChildren<CheckpointManager>();

        playerRig = inputDetection.VRMode ? inputDetection.XRRig : inputDetection.DesktopRig;
        playerRigDefaultPosition = playerRig.transform.position;

        startMask = gameCamera.GetComponent<Camera>().cullingMask;

        AudioManager.instance.Play("LabAmbience01");        
    }

    private void Update()
    {
        if(blackScreenToggle && blackOverlay.alpha == 0) //If trying to be on and the alpha is off
        {
            StartCoroutine(fade(0, 1)); //Initiate fade in
        }
        else if(!blackScreenToggle && blackOverlay.alpha == 1) //If trying to be off and the alpha is on
        {
            StartCoroutine(fade(1, 0)); //Initiate fade out
        }
    }

    IEnumerator fade(float startValue, float endValue)
    {
        float timeElapsed = 0;

        while(timeElapsed < blinkDuration)
        {
            blackOverlay.alpha = Mathf.Lerp(startValue, endValue, timeElapsed / blinkDuration);

            timeElapsed += Time.deltaTime;

            yield return null;
        }

        blackOverlay.alpha = endValue;
        
        yield return new WaitForSeconds(0.2f);
        
        if(endValue == 1)
        {
            screenBlackedOut = true;
        }
        else
        {
            screenBlackedOut = false;
        }
    }


    public void QuitApplication()
    {
        Application.Quit();
    }

    public void ZoomToggleChange(bool value)
    {
        teleportToggle = !value;
    }
}
