using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DNAMinigameManager : MonoBehaviour
{
    //Put in a local canvas
    [SerializeField]
    Dropdown dd;

    public GameObject DNAGroup;

    public List<DNAPosition> dnaPositions;

    public int currentPosition;

    public bool currentlyTransfering = false;

    public Text scoreText;
    public int score;
    public int highScore;
    public Text HighscoreText;

    public GameObject dnaObservationPosition;

    private void Start()
    {
        foreach (Transform child in DNAGroup.transform)
        {
            dnaPositions.Add(child.GetComponent<DNAPosition>());
        }

        NextPosition();
    }

    public void DropDownChange()
    {
        if(dd.value == dnaPositions[currentPosition].DNAPositionType)
        {
            AddScore();
        }
        else
        {
            RemoveScore();
        }
        dd.interactable = false;
        NextPosition();
    }

    public void NextPosition()
    {
        if (!currentlyTransfering) //if not currently transfering
        {
            currentPosition++;
            if (currentPosition > dnaPositions.Count) //If we are outside this subgroup
            {
                currentPosition = 0;
            }

            currentlyTransfering = true;
            StartCoroutine("MoveToNewPosition");
        }
    }

    public void AddScore()
    {
        score++;
        scoreText.text = "Score : " + score.ToString();
        //Play get score particle on text
    }

    public void RemoveScore()
    {
        if(score > highScore)
        {
            highScore = score;
            HighscoreText.text = "Highscore" + highScore.ToString();
            //Play get high score particle
        }
        score = 0;
        //Play lose score particle
    }

    IEnumerator MoveToNewPosition()
    {
        float timeElapsed = 0;
        float transitionDuration = 2;

        dnaPositions[currentPosition].transform.parent = this.gameObject.transform;
        DNAGroup.transform.SetParent(dnaPositions[currentPosition].transform, true);

        Vector3 startValue = dnaPositions[currentPosition].transform.position;
        Vector3 endValue = dnaObservationPosition.transform.position;

        while (timeElapsed < transitionDuration)
        {
            //valueToLerp = Mathf.Lerp(startValue, endValue, timeElapsed / lerpDuration)
            dnaPositions[currentPosition].transform.position = Vector3.Lerp(startValue, endValue, timeElapsed / transitionDuration);

            timeElapsed += Time.deltaTime;

            yield return null;
        }

        dnaPositions[currentPosition].transform.position = endValue;
        DNAGroup.transform.parent = this.gameObject.transform;
        dnaPositions[currentPosition].transform.SetParent(DNAGroup.transform, true);

        currentlyTransfering = false;
        dd.interactable = true;
    }
}
