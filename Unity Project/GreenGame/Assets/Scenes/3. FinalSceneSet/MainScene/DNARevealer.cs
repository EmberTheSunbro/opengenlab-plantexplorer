using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DNARevealer : MonoBehaviour
{
    int DNAIndex = 1;

    float nextDNARevealTime = 1;

    public bool isDNARevealing = false;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("SetActiveDNA", 1);
    }



    void SetActiveDNA()
    {
        transform.GetChild(DNAIndex).transform.gameObject.SetActive(true);

        DNAIndex++;

        if (DNAIndex < transform.childCount)
        {
            StartCoroutine(DNARevealCoroutine());
        }
        else
        {
            foreach (Transform child in transform)
            {
                child.transform.gameObject.SetActive(false);

            }

            transform.GetChild(0).transform.gameObject.SetActive(true);

            DNAIndex = 1;
            nextDNARevealTime = 0.4f;

            StartCoroutine(DNARevealCoroutine2());
        }
    }

    IEnumerator DNARevealCoroutine()
    {
        yield return new WaitForSeconds(nextDNARevealTime);
        SetActiveDNA();

        if (DNAIndex >= 5)
        {
            nextDNARevealTime = 0.0075f;
        }
        else
        {
            nextDNARevealTime -= 0.05f;
        }
    }

    IEnumerator DNARevealCoroutine2()
    {
        yield return new WaitForSeconds(1);
        SetActiveDNA();

        nextDNARevealTime -= 0.010f;

    }
}
