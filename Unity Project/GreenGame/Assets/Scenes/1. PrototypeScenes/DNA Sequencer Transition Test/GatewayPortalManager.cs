using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GatewayPortalManager : MonoBehaviour
{
    public GameObject gateway;

    [SerializeField]
    Transform surfacePoint;

    GameObject gatewayInstance;

    bool dash;

    float startTime, distance;

    float currentSpeed;

    float exitSpeed;

    float entrySpeed;

    Vector3 startPos, endPos;

    [SerializeField]
    AnimationCurve speedCruve;

    [SerializeField]
    BoxCollider boundingBox;

    bool disableMesh;

    // Start is called before the first frame update
    void Start()
    {
        exitSpeed = boundingBox.size.y * 2;

        entrySpeed = exitSpeed * 2;

    }

    // Update is called once per frame
    void Update()
    {       

        //if (Input.GetKeyDown(KeyCode.S))
        //{
        //    PullIn();
        //}

        //if (Input.GetKeyDown(KeyCode.W))
        //{
        //    PushOut();
        //}

        if (dash)
        {
            float percent = (Time.time - startTime) / (distance / currentSpeed);

            transform.position = Vector3.Lerp(startPos, endPos, speedCruve.Evaluate(percent));

            if (percent >= 1)
            {
                dash = false;

                if (disableMesh)
                {
                    //GetComponent<MeshRenderer>().enabled = false;
                    foreach (Renderer r in GetComponentsInChildren<Renderer>())
                        r.enabled = false;
                }

            }
        }
    }

    public void PullIn()
    {
        if (gatewayInstance == null)
            gatewayInstance = Instantiate(gateway, new Vector3(transform.position.x, surfacePoint.transform.position.y, transform.position.z), Quaternion.identity);

        gatewayInstance.transform.eulerAngles = new Vector3(gatewayInstance.transform.rotation.x, transform.eulerAngles.y, gatewayInstance.transform.rotation.z);

        gatewayInstance.transform.localScale = new Vector3((boundingBox.size.x * 1f), gatewayInstance.transform.localScale.y, (boundingBox.size.z * 1f));

        gatewayInstance.transform.GetChild(0).GetComponent<Animator>().SetTrigger("Pull");

        currentSpeed = exitSpeed;

        disableMesh = true;

        StartCoroutine(MoveObjectCoroutine(transform.position - Vector3.up * boundingBox.size.y - Vector3.up * 0.1f));
    }

    public void PushOut()
    {
        //    if (gatewayInstance == null)
        //        gatewayInstance = Instantiate(gateway, new Vector3(transform.position.x, surfacePoint.transform.position.y, transform.position.z), transform.rotation);
        gatewayInstance = Instantiate(gateway);
        Vector3 newPos = transform.position;
        newPos.y = surfacePoint.transform.position.y;

        gatewayInstance.transform.position = newPos;

        //Debug.Log(gatewayInstance.transform.position);
        //Debug.Log(transform.position);

        gatewayInstance.transform.localScale = new Vector3((boundingBox.size.x * 0.8f) + 0.1f, gatewayInstance.transform.localScale.y, (boundingBox.size.z * 0.8f) + 0.1f);

        gatewayInstance.transform.GetChild(0).GetComponent<Animator>().SetTrigger("Push");

        currentSpeed = entrySpeed;

        disableMesh = false;
        //GetComponent<MeshRenderer>().enabled = true;
        foreach (Renderer r in GetComponentsInChildren<Renderer>())
            r.enabled = true;    

        StartCoroutine(MoveObjectCoroutine(transform.position + Vector3.up * boundingBox.size.y + Vector3.up * 0.1f));
    }

    IEnumerator MoveObjectCoroutine(Vector3 finalPosition)
    {
        yield return new WaitForSeconds(0.75f);

        dash = true;

        startTime = Time.time;

        startPos = transform.position;

        endPos = finalPosition;

        distance = Vector3.Distance(startPos, endPos);

    }


}
